package com.globalia.application.repository.group;

import com.globalia.Errors;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.GroupItem;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class GroupsRedis extends FindAllRedis<GroupItem> {

	@Value("${redis.groupsKey}")
	private String groupsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public AllItemsResponse getAllItems(final GroupItem item, final Monitor monitor) {
		return this.getAllGroups(this.redisAccess.getRediskey(this.groupsKey), item.getEnvironment(), monitor);
	}

	private AllItemsResponse getAllGroups(final String redisKey, final String filter, final Monitor monitor) {
		AllItemsResponse groups = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addGroups((String) entry.getValue(), groups, filter);
			}

			if (groups.getGroups() == null) {
				groups.setError(this.errors.addError(HttpStatus.NO_CONTENT, "Cannot find groups", monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
			}
		} catch (RuntimeException r) {
			groups.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return groups;
	}

	private void addGroups(final String json, final AllItemsResponse groups, final String filter) {
		try {
			GroupItem group = (GroupItem) getJsonHandler().fromJson(json, GroupItem.class);
			if (!group.isDeleted() && (group.getEnvironment() == null || !StringUtils.hasText(filter) || filter.equalsIgnoreCase(group.getEnvironment()))) {
				if (groups.getGroups() == null) {
					groups.setGroups(new LinkedHashSet<>());
				}
				groups.getGroups().add(group);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}