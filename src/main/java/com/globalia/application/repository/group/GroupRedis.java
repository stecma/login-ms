package com.globalia.application.repository.group;

import com.globalia.Errors;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class GroupRedis extends FindRedis<GroupItem> {

	@Value("${redis.groupsKey}")
	private String groupsKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final GroupItem item, final Monitor monitor) {
		return this.getGroup(this.redisAccess.getRediskey(this.groupsKey), item.getId(), monitor);
	}

	private ItemResponse getGroup(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse item = new ItemResponse();
		try {
			item.setGroup((GroupItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), GroupItem.class));
			if (item.getGroup() == null || item.getGroup().isDeleted()) {
				item.setError(this.errors.addError(String.format("Cannot find group with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			item.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			item.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return item;
	}
}