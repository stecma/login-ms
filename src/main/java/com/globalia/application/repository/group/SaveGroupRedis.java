package com.globalia.application.repository.group;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.LinkedHashSet;
import java.util.UUID;

@Repository
@RefreshScope
public class SaveGroupRedis extends AbstractSaveRedis<GroupItem> {

	@Value("${redis.groupsKey}")
	private String groupsKey;

	@Override
	protected String getItemAction(final GroupItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(final GroupItem item, final ItemResponse response, final String json) {
		getClient().add(getRedisAccess().getRediskey(this.groupsKey), item.getId(), json);
		response.setGroup(item);
	}

	@Override
	protected String getEntity(final GroupItem item) {
		return "group";
	}

	private void addInternalCodes(final GroupItem item) {
		LinkedHashSet<PairValue> values = new LinkedHashSet<>();
		if (addServiceInternalCodes(item.getServices(), values)) {
			item.setServices(null);
		}

		if (!values.isEmpty()) {
			item.setCodes(values);
		}
	}
}