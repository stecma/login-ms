package com.globalia.application.repository.master;

import com.globalia.Errors;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class MasterRedis extends FindRedis<MasterItem> {

	private static final String REDIS_KEY_FORMAT = "%s:%s";
	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errorComponent;

	public ItemResponse getItem(final MasterItem item, final Monitor monitor) {
		return this.getMaster(String.format(MasterRedis.REDIS_KEY_FORMAT, this.redisAccess.getRediskey(this.mastersKey), item.getEntity().toUpperCase()), item.getId(), monitor);
	}

	private ItemResponse getMaster(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse master = new ItemResponse();
		try {
			master.setMaster((MasterItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), MasterItem.class));
			if (master.getMaster() == null || master.getMaster().isDeleted()) {
				master.setError(this.errorComponent.addError(HttpStatus.NO_CONTENT, String.format("Cannot find master with id: %s", key), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
			}
		} catch (RuntimeException r) {
			master.setError(this.errorComponent.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			master.setError(this.errorComponent.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return master;
	}
}