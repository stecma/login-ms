package com.globalia.application.repository.master;

import com.globalia.Errors;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Repository
@RefreshScope
public class MastersRedis extends FindAllRedis<MasterItem> {

	private static final String REDIS_KEY_FORMAT = "%s:%s";
	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public AllItemsResponse getAllItems(final MasterItem item, final Monitor monitor) {
		String redisKey = this.redisAccess.getRediskey(this.mastersKey);
		String masterKey = String.format("%s:*", redisKey);
		if (StringUtils.hasText(item.getEntity())) {
			masterKey = String.format(MastersRedis.REDIS_KEY_FORMAT, redisKey, item.getEntity().toUpperCase());
		}

		return this.getAllMasters(masterKey, monitor);
	}

	public Set<String> findCodes(final String entity) {
		Set<String> codes = null;
		try {
			codes = getClient().keys(String.format(MastersRedis.REDIS_KEY_FORMAT, this.redisAccess.getRediskey(this.mastersKey), entity));
		} catch (RuntimeException ignored) {
			//
		}
		return codes;
	}

	private AllItemsResponse getAllMasters(final String redisKey, final Monitor monitor) {
		AllItemsResponse masters = new AllItemsResponse();
		try {
			Set<String> codes = getClient().patternKeys(redisKey);
			for (String code : codes) {
				Map<String, Object> map = getClient().getAll(code);
				for (Map.Entry<String, Object> entry : map.entrySet()) {
					this.addMaster((String) entry.getValue(), masters);
				}
			}

			if (masters.getMasters() == null) {
				masters.setError(this.errors.addError("Cannot find services", ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			masters.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return masters;
	}

	private void addMaster(final String json, final AllItemsResponse masters) {
		try {
			MasterItem master = (MasterItem) getJsonHandler().fromJson(json, MasterItem.class);
			if (!master.isDeleted()) {
				if (masters.getMasters() == null) {
					masters.setMasters(new LinkedHashSet<>());
				}
				masters.getMasters().add(master);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}