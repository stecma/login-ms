package com.globalia.application.repository.master;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

@Repository
@RefreshScope
public class SaveMasterRedis extends AbstractSaveRedis<MasterItem> {

	private static final String REDIS_KEY_FORMAT = "%s:%s";
	@Value("${redis.mastersKey}")
	private String mastersKey;

	@Override
	protected String getItemAction(final MasterItem item) {
		if (item.isCreated()) {
			return AbstractSaveRedis.CRE_ACTION;
		}
		if (item.isDeleted()) {
			return AbstractSaveRedis.DEL_ACTION;
		}
		return AbstractSaveRedis.UPD_ACTION;
	}

	@Override
	protected void getResponse(final MasterItem item, final ItemResponse response, final String json) {
		getClient().add(String.format(SaveMasterRedis.REDIS_KEY_FORMAT, getRedisAccess().getRediskey(this.mastersKey), item.getEntity().toUpperCase()), item.getId(), json);
		response.setMaster(item);
	}

	@Override
	protected String getEntity(final MasterItem item) {
		return "master";
	}
}