package com.globalia.application.repository.service;

import com.globalia.Errors;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class ServicesRedis extends FindAllRedis<ServiceItem> {

	@Value("${redis.servicesKey}")
	private String servicesKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public AllItemsResponse getAllItems(final ServiceItem item, final Monitor monitor) {
		return this.getAllServices(this.redisAccess.getRediskey(this.servicesKey), item.getEnvironment(), monitor);
	}

	private AllItemsResponse getAllServices(final String redisKey, final String filter, final Monitor monitor) {
		AllItemsResponse services = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addService((String) entry.getValue(), services, filter);
			}

			if (services.getServices() == null) {
				services.setError(this.errors.addError("Cannot find services", ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			services.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return services;
	}

	private void addService(final String json, final AllItemsResponse services, final String filter) {
		try {
			ServiceItem service = (ServiceItem) getJsonHandler().fromJson(json, ServiceItem.class);
			if (!service.isDeleted() && (service.getEnvironment() == null || !StringUtils.hasText(filter) || filter.equalsIgnoreCase(service.getEnvironment()))) {
				if (services.getServices() == null) {
					services.setServices(new LinkedHashSet<>());
				}
				services.getServices().add(service);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}