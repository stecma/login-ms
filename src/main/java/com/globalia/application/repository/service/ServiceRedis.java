package com.globalia.application.repository.service;

import com.globalia.Errors;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class ServiceRedis extends FindRedis<ServiceItem> {

	@Value("${redis.servicesKey}")
	private String servicesKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final ServiceItem item, final Monitor monitor) {
		return this.getService(this.redisAccess.getRediskey(this.servicesKey), item.getId(), monitor);
	}

	private ItemResponse getService(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse response = new ItemResponse();
		try {
			response.setService((ServiceItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), ServiceItem.class));
			if (response.getService() == null || response.getService().isDeleted()) {
				response.setError(this.errors.addError(String.format("Cannot find service with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			response.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return response;
	}
}