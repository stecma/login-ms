package com.globalia.application.repository.service;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.ServiceItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Repository
@RefreshScope
public class SaveServiceRedis extends AbstractSaveRedis<ServiceItem> {

	@Value("${redis.servicesKey}")
	private String servicesKey;

	@Override
	protected String getItemAction(ServiceItem item) {
		String action = getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		return action;
	}

	@Override
	protected void getResponse(ServiceItem item, ItemResponse response, String json) {
		getClient().add(getRedisAccess().getRediskey(this.servicesKey), item.getId(), json);
		response.setService(item);
	}

	@Override
	protected String getEntity(ServiceItem item) {
		return "service";
	}
}