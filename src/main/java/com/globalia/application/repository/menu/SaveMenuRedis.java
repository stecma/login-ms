package com.globalia.application.repository.menu;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MenuItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Repository
@RefreshScope
public class SaveMenuRedis extends AbstractSaveRedis<MenuItem> {

	@Value("${redis.menusKey}")
	private String menusKey;

	@Override
	protected String getItemAction(MenuItem item) {
		String action = this.getAction(item.getId(), item.isDeleted());
		if (!StringUtils.hasText(item.getId())) {
			item.setId(UUID.randomUUID().toString());
		}
		this.addInternalCodes(item);
		return action;
	}

	@Override
	protected void getResponse(MenuItem item, ItemResponse response, String json) {
		getClient().add(getRedisAccess().getRediskey(this.menusKey), item.getId(), json);
		response.setMenu(item);
	}

	@Override
	protected String getEntity(MenuItem item) {
		return "menu";
	}

	public int getOrder() {
		Set<String> keys = getClient().keys(getRedisAccess().getRediskey(this.menusKey));
		return keys.size() + 1;
	}

	private void addInternalCodes(final MenuItem item) {
		LinkedHashSet<PairValue> values = new LinkedHashSet<>();
		if (item.getGroups() != null && !item.getGroups().isEmpty()) {
			LinkedHashSet<String> codes = new LinkedHashSet<>();
			for (GroupItem group : item.getGroups()) {
				codes.add(group.getId());
			}

			if (!codes.isEmpty()) {
				values.add(addPairValue("groups", codes));
				item.setGroups(null);
			}
		}

		if (addServiceInternalCodes(item.getServices(), values)) {
			item.setServices(null);
		}

		if (!values.isEmpty()) {
			item.setCodes(values);
		}
	}
}