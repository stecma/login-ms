package com.globalia.application.repository.menu;

import com.globalia.Errors;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.MenuItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class MenusRedis extends FindAllRedis<MenuItem> {

	@Value("${redis.menusKey}")
	private String menusKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public AllItemsResponse getAllItems(final MenuItem item, final Monitor monitor) {
		return this.getAllMenus(this.redisAccess.getRediskey(this.menusKey), item.getEnvironment(), item.getCompany(), monitor);
	}

	private AllItemsResponse getAllMenus(final String redisKey, final String environment, final String company, final Monitor monitor) {
		AllItemsResponse menus = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addMenu((String) entry.getValue(), menus, environment, company);
			}

			if (menus.getMenus() == null) {
				menus.setError(this.errors.addError("Cannot find menus", ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			menus.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return menus;
	}

	private void addMenu(final String json, final AllItemsResponse menus, final String environment, final String company) {
		MenuItem menu;
		try {
			menu = (MenuItem) getJsonHandler().fromJson(json, MenuItem.class);
			if (this.isValid(menu, environment, company)) {
				if (menus.getMenus() == null) {
					menus.setMenus(new LinkedHashSet<>());
				}
				menus.getMenus().add(menu);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}

	private boolean isValid(final MenuItem menu, final String environment, final String company) {
		return !menu.isDeleted() && (menu.getEnvironment() == null || !StringUtils.hasText(environment) || environment.equalsIgnoreCase(menu.getEnvironment()))
				&& (menu.getCompany() == null || !StringUtils.hasText(company) || company.equalsIgnoreCase(menu.getCompany()));
	}
}