package com.globalia.application.repository.menu;

import com.globalia.Errors;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MenuItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class MenuRedis extends FindRedis<MenuItem> {

	@Value("${redis.menusKey}")
	private String menusKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final MenuItem item, final Monitor monitor) {
		return this.getMenu(this.redisAccess.getRediskey(this.menusKey), item.getId(), monitor);
	}

	private ItemResponse getMenu(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse item = new ItemResponse();
		try {
			item.setMenu((MenuItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), MenuItem.class));
			if (item.getMenu() == null || item.getMenu().isDeleted()) {
				item.setError(this.errors.addError(String.format("Cannot find menu with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			item.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			item.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}

		return item;
	}
}