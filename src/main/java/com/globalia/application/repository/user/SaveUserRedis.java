package com.globalia.application.repository.user;

import com.globalia.application.AbstractSaveRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.UserItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.dto.login.UserMenuCodes;
import com.globalia.enumeration.login.MasterType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashSet;
import java.util.Set;

@Repository
@RefreshScope
public class SaveUserRedis extends AbstractSaveRedis<UserItem> {

	@Value("${redis.usersKey}")
	private String usersKey;

	@Override
	protected String getItemAction(final UserItem item) {
		this.addInternalCodes(item);
		this.addInternalMenuCodes(item);

		if (item.isCreated()) {
			return AbstractSaveRedis.CRE_ACTION;
		}
		if (item.isDeleted()) {
			return AbstractSaveRedis.DEL_ACTION;
		}
		return AbstractSaveRedis.UPD_ACTION;
	}

	@Override
	protected void getResponse(final UserItem item, final ItemResponse response, final String json) {
		getClient().add(getRedisAccess().getRediskey(this.usersKey), item.getId(), json);
		response.setUser(item);
	}

	@Override
	protected String getEntity(final UserItem item) {
		return "user";
	}

	private void addInternalCodes(final UserItem item) {
		LinkedHashSet<PairValue> values = new LinkedHashSet<>();
		this.addInternalCodesList(values, MasterType.COMPANY, item.getCompanies(), item);
		this.addInternalCodesList(values, MasterType.ENVIRONMENT, item.getEnvironments(), item);
		item.setCodes(values);
	}

	private void addInternalCodesList(final Set<PairValue> values, final MasterType masterType, final Set<MasterItem> masters, final UserItem item) {
		if (masters != null && !masters.isEmpty()) {
			LinkedHashSet<String> codes = new LinkedHashSet<>();
			for (MasterItem master : masters) {
				codes.add(master.getId());
			}
			values.add(addPairValue(masterType.name(), codes));
			if (masterType == MasterType.COMPANY) {
				item.setCompanies(null);
			} else if (masterType == MasterType.ENVIRONMENT) {
				item.setEnvironments(null);
			}
		}
	}

	private void addInternalMenuCodes(final UserItem user) {
		if (user != null && user.getConfiguration() != null) {
			for (UserMenu config : user.getConfiguration()) {
				config.setCodes(this.getCodes(config));
				config.setMenus(null);
			}
		}
	}

	private LinkedHashSet<UserMenuCodes> getCodes(final UserMenu config) {
		LinkedHashSet<UserMenuCodes> codes = null;
		if (config.getMenus() != null && !config.getMenus().isEmpty()) {
			for (MenuItem menu : config.getMenus()) {
				if (codes == null) {
					codes = new LinkedHashSet<>();
				}
				this.getGroups(codes, menu.getId(), menu.getGroups());
				this.getServices(codes, menu.getId(), menu.getServices());
			}
		}
		return codes;
	}

	private void getGroups(final LinkedHashSet<UserMenuCodes> codes, final String menuId, final LinkedHashSet<GroupItem> groups) {
		if (groups != null && !groups.isEmpty()) {
			for (GroupItem group : groups) {
				if (group.getServices() != null && !group.getServices().isEmpty()) {
					for (ServiceItem service : group.getServices()) {
						codes.add(this.addCodes(menuId, group.getId(), service.getId(), (service.getUrl() != null) && service.getUrl().isReadOnly()));
					}
				}
			}
		}
	}

	private void getServices(final LinkedHashSet<UserMenuCodes> codes, final String menuId, final LinkedHashSet<ServiceItem> services) {
		if (services != null && !services.isEmpty()) {
			for (ServiceItem service : services) {
				codes.add(this.addCodes(menuId, null, service.getId(), service.getUrl().isReadOnly()));
			}
		}
	}

	private UserMenuCodes addCodes(final String menuId, final String groupId, final String serviceId, final boolean readOnly) {
		UserMenuCodes code = new UserMenuCodes();
		code.setMenu(menuId);
		code.setGroup(groupId);
		code.setService(serviceId);
		code.setReadOnly(readOnly);
		return code;
	}
}