package com.globalia.application.repository.user;

import com.globalia.Errors;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.UserItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RefreshScope
public class UserRedis extends FindRedis<UserItem> {

	@Value("${redis.usersKey}")
	private String usersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	public ItemResponse getItem(final UserItem item, final Monitor monitor) {
		return this.getUser(this.redisAccess.getRediskey(this.usersKey), item.getId(), monitor);
	}

	private ItemResponse getUser(final String redisKey, final String key, final Monitor monitor) {
		ItemResponse user = new ItemResponse();
		try {
			user.setUser((UserItem) getJsonHandler().fromJson((String) getClient().get(redisKey, key), UserItem.class));

			if (user.getUser() == null || user.getUser().isDeleted()) {
				user.setError(this.errors.addError(String.format("Cannot find user with id: %s", key), ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			user.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		} catch (IOException e) {
			user.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format("Unknown error during object serialization %s", e.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return user;
	}
}