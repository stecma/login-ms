package com.globalia.application.repository.user;

import com.globalia.Errors;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.UserItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.monitoring.Monitor;
import com.globalia.redis.FindAllRedis;
import com.globalia.redis.RedisAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

@Repository
@RefreshScope
public class UsersRedis extends FindAllRedis<UserItem> {

	@Value("${redis.usersKey}")
	private String usersKey;

	@Autowired
	private RedisAccess redisAccess;
	@Autowired
	private Errors errors;

	@Override
	public AllItemsResponse getAllItems(final UserItem item, final Monitor monitor) {
		return this.getAllUsers(this.redisAccess.getRediskey(this.usersKey), monitor);
	}

	private AllItemsResponse getAllUsers(final String redisKey, final Monitor monitor) {
		AllItemsResponse users = new AllItemsResponse();
		try {
			Map<String, Object> map = getClient().getAll(redisKey);
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				this.addUser((String) entry.getValue(), users);
			}

			if (users.getUsers() == null) {
				users.setError(this.errors.addError("Cannot find users", ErrorLayer.REPOSITORY_LAYER, monitor));
			}
		} catch (RuntimeException r) {
			users.setError(this.errors.addError(HttpStatus.INTERNAL_SERVER_ERROR, String.format(Errors.EXCEPTION_MSG, r.getLocalizedMessage()), monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber()));
		}
		return users;
	}

	private void addUser(final String json, final AllItemsResponse users) {
		try {
			UserItem user = (UserItem) getJsonHandler().fromJson(json, UserItem.class);
			if (!user.isDeleted()) {
				if (users.getUsers() == null) {
					users.setUsers(new LinkedHashSet<>());
				}
				users.getUsers().add(user);
			}
		} catch (IOException ignored) {
			// Ignored exception
		}
	}
}