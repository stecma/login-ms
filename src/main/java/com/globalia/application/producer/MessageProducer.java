package com.globalia.application.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.dto.KafkaItem;
import com.globalia.json.JsonHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Component
@RefreshScope
@Slf4j
public class MessageProducer {

	@Value("${kafka.topicname}")
	private String topicName;
	@Value("${kafka.topicreplyname}")
	private String replyTopic;

	@Autowired
	private ReplyingKafkaTemplate<String, String, String> replyKafkaTemplate;
	@Autowired
	private JsonHandler jsonHandler;

	public String reply(final KafkaItem message) {
		try {
			return this.getResponse(this.getConsumerReply(message.getKey(), this.getMessage(message)));
		} catch (JsonProcessingException e) {
			MessageProducer.log.info(String.format("Error sending message %s", e.getMessage()));
		} catch (Exception e) {
			MessageProducer.log.info(String.format("Unkown message %s", e.getMessage()));
		}
		return null;
	}

	private String getMessage(final KafkaItem message) throws JsonProcessingException {
		MessageProducer.log.info(String.format("sending JSON data='%s' to topic='%s'", message, this.topicName));
		String msgStr = this.jsonHandler.toJson(message);
		MessageProducer.log.info(String.format("key='%s' message=%s", message.getKey(), msgStr));
		return msgStr;
	}

	private String getConsumerReply(final String key, final String message) throws ExecutionException, InterruptedException, TimeoutException {
		ProducerRecord<String, String> record = new ProducerRecord<>(this.topicName, key, message);
		record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, this.replyTopic.getBytes()));
		MessageProducer.log.info(String.format("TransactionID %s produced record on topic %s", key, this.replyTopic));
		RequestReplyFuture<String, String, String> sendAndReceive = this.replyKafkaTemplate.sendAndReceive(record);
		return sendAndReceive.get(1, TimeUnit.SECONDS).value();
	}

	String getResponse(final String reply) throws IOException {
		KafkaItem item = (KafkaItem) this.jsonHandler.fromJson(reply, KafkaItem.class);
		if (item == null) {
			return null;
		}
		return item.getJson();
	}
}