package com.globalia.application.service.group;

import com.globalia.application.repository.group.GroupRedis;
import com.globalia.application.service.service.Services;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;

@Service
public class Group extends FindService<GroupItem> {

	@Autowired
	private GroupRedis groupRedis;
	@Autowired
	private Services services;

	public ItemResponse getItem(final GroupItem item, final Monitor monitor) {
		ItemResponse response = this.groupRedis.getItem(item, monitor);
		if (response.getError() == null) {
			this.completeGroup(response.getGroup(), monitor);
		}
		return response;
	}

	private void completeGroup(final GroupItem group, final Monitor monitor) {
		if (group.getCodes() != null && !group.getCodes().isEmpty()) {
			group.setServices((LinkedHashSet<ServiceItem>) this.services.addServices(group.getCodes(), this.services.getAllServices(monitor)));
			group.setCodes(null);
		}
	}
}