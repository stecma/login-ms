package com.globalia.application.service.group;

import com.globalia.application.repository.group.SaveGroupRedis;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveGroup extends SaveService<GroupItem> {

	@Autowired
	private SaveGroupRedis groupRedis;

	public ItemResponse createItem(final GroupItem item, final Monitor monitor) {
		return this.groupRedis.addItem(item, monitor);
	}

	public ItemResponse updateItem(final GroupItem item, final Monitor monitor) {
		return this.groupRedis.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final GroupItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.groupRedis.addItem(item, monitor);
	}
}