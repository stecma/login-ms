package com.globalia.application.service.group;

import com.globalia.application.repository.group.GroupsRedis;
import com.globalia.application.service.service.Services;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Service
public class Groups extends FindAllService<GroupItem> {

	@Autowired
	private GroupsRedis groupRedis;
	@Autowired
	private Services services;

	public AllItemsResponse getAllItems(final GroupItem item, final Monitor monitor) {
		return this.groupRedis.getAllItems(item, monitor);
	}

	public Set<GroupItem> addGroups(final Set<PairValue> codes, final Map<String, GroupItem> grpMap, Map<String, ServiceItem> srvMap) {
		LinkedHashSet<GroupItem> groups = new LinkedHashSet<>();
		if (codes != null) {
			for (PairValue pair : codes) {
				if (!"groups".equalsIgnoreCase(pair.getKey())) {
					continue;
				}

				for (String code : pair.getValue()) {
					if (grpMap.containsKey(code)) {
						GroupItem group = createNewGroup(grpMap.get(code));
						group.setServices((LinkedHashSet<ServiceItem>) this.services.addServices(group.getCodes(), srvMap));
						group.setCodes(null);
						groups.add(group);
					}
				}
			}
		}
		return groups.isEmpty() ? null : groups;
	}

	public Map<String, GroupItem> getAllGroups(final Monitor monitor) {
		Map<String, GroupItem> map = new HashMap<>();
		AllItemsResponse allItems = this.groupRedis.getAllItems(new GroupItem(), monitor);
		if (allItems.getError() == null) {
			for (GroupItem group : allItems.getGroups()) {
				map.put(group.getId(), group);
			}
		}
		return map;
	}

	public GroupItem createNewGroup(final GroupItem mappedGroup) {
		GroupItem group = new GroupItem();
		group.setId(mappedGroup.getId());
		group.setNames(mappedGroup.getNames());
		group.setDescriptions(mappedGroup.getDescriptions());
		group.setActive(mappedGroup.isActive());
		group.setDeleted(mappedGroup.isDeleted());
		group.setEnvironment(mappedGroup.getEnvironment());
		group.setIcon(mappedGroup.getIcon());
		group.setServices(mappedGroup.getServices());
		group.setCodes(mappedGroup.getCodes());
		return group;
	}
}