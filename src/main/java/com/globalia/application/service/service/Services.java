package com.globalia.application.service.service;

import com.globalia.application.repository.service.ServicesRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Service
public class Services extends FindAllService<ServiceItem> {

	@Autowired
	private ServicesRedis serviceRedis;

	public AllItemsResponse getAllItems(final ServiceItem item, final Monitor monitor) {
		return this.serviceRedis.getAllItems(item, monitor);
	}

	public Set<ServiceItem> addServices(final Set<PairValue> codes, final Map<String, ServiceItem> map) {
		LinkedHashSet<ServiceItem> services = new LinkedHashSet<>();
		if (codes != null) {
			for (PairValue pair : codes) {
				if (!"services".equalsIgnoreCase(pair.getKey())) {
					continue;
				}
				for (String code : pair.getValue()) {
					if (map.containsKey(code)) {
						services.add(map.get(code));
					}
				}
			}
		}
		return services.isEmpty() ? null : services;
	}

	public Map<String, ServiceItem> getAllServices(final Monitor monitor) {
		Map<String, ServiceItem> map = new HashMap<>();
		AllItemsResponse allItems = this.getAllItems(new ServiceItem(), monitor);
		if (allItems.getError() == null) {
			for (ServiceItem service : allItems.getServices()) {
				map.put(service.getId(), service);
			}
		}
		return map;
	}
}