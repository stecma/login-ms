package com.globalia.application.service.service;

import com.globalia.application.repository.service.ServiceRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service extends FindService<ServiceItem> {

	@Autowired
	private ServiceRedis serviceRedis;

	public ItemResponse getItem(final ServiceItem item, final Monitor monitor) {
		return this.serviceRedis.getItem(item, monitor);
	}
}