package com.globalia.application.service.service;

import com.globalia.application.repository.service.SaveServiceRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceSave extends SaveService<ServiceItem> {

	@Autowired
	private SaveServiceRedis serviceRedis;

	public ItemResponse createItem(final ServiceItem item, final Monitor monitor) {
		return this.serviceRedis.addItem(item, monitor);
	}

	public ItemResponse updateItem(final ServiceItem item, final Monitor monitor) {
		return this.serviceRedis.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final ServiceItem item, final Monitor monitor) {
		return this.serviceRedis.addItem(item, monitor);
	}
}