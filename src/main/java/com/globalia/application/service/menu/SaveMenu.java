package com.globalia.application.service.menu;

import com.globalia.application.repository.menu.SaveMenuRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MenuItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveMenu extends SaveService<MenuItem> {

	@Autowired
	private SaveMenuRedis menuRedis;

	public ItemResponse createItem(final MenuItem item, final Monitor monitor) {
		item.setOrder(this.menuRedis.getOrder());
		return this.menuRedis.addItem(item, monitor);
	}

	public ItemResponse updateItem(final MenuItem item, final Monitor monitor) {
		return this.menuRedis.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final MenuItem item, final Monitor monitor) {
		return this.menuRedis.addItem(item, monitor);
	}
}