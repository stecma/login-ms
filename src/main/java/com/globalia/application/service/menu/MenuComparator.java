package com.globalia.application.service.menu;

import com.globalia.dto.login.MenuItem;

import java.util.Comparator;

public class MenuComparator implements Comparator<MenuItem> {

	@Override
	public int compare(final MenuItem menu1, final MenuItem menu2) {
		return Integer.compare(menu1.getOrder(), menu2.getOrder());
	}
}
