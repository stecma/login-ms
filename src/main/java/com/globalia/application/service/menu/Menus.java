package com.globalia.application.service.menu;

import com.globalia.application.repository.menu.MenusRedis;
import com.globalia.application.service.service.Services;
import com.globalia.application.service.group.Groups;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class Menus extends FindAllService<MenuItem> {

	@Autowired
	private MenusRedis menuRedis;
	@Autowired
	private Services services;
	@Autowired
	private Groups groups;

	public AllItemsResponse getAllItems(final MenuItem item, final Monitor monitor) {
		AllItemsResponse menus = this.menuRedis.getAllItems(item, monitor);
		if (menus.getError() == null) {
			Map<String, ServiceItem> srvMap = this.services.getAllServices(monitor);
			Map<String, GroupItem> grpMap = this.groups.getAllGroups(monitor);
			for (MenuItem menu : menus.getMenus()) {
				if (!srvMap.isEmpty() && !grpMap.isEmpty()) {
					this.completeMenu(menu, srvMap, grpMap);
				}
			}
			menus.setMenus(new LinkedHashSet<>(this.resultSorted(menus.getMenus())));
		}
		return menus;
	}

	private void completeMenu(final MenuItem menu, final Map<String, ServiceItem> srvMap, final Map<String, GroupItem> grpMap) {
		if (menu.getCodes() != null && !menu.getCodes().isEmpty()) {
			menu.setServices((LinkedHashSet<ServiceItem>) this.services.addServices(menu.getCodes(), srvMap));
			menu.setGroups((LinkedHashSet<GroupItem>) this.groups.addGroups(menu.getCodes(), grpMap, srvMap));
			menu.setCodes(null);
		}
	}

	private List<MenuItem> resultSorted(final Set<MenuItem> masters) {
		List<MenuItem> list = new ArrayList<>(masters);
		list.sort(new MenuComparator());
		return list;
	}

	public Map<String, MenuItem> getAllMenus(final Monitor monitor) {
		AllItemsResponse menuItems = this.menuRedis.getAllItems(new MenuItem(), monitor);
		Map<String, MenuItem> map = new HashMap<>();
		if (menuItems.getError() == null) {
			for (MenuItem menu : menuItems.getMenus()) {
				map.put(menu.getId(), menu);
			}
		}
		return map;
	}

	public MenuItem createNewMenu(final MenuItem mappedMenu) {
		MenuItem menu = new MenuItem();
		menu.setEnvironment(mappedMenu.getEnvironment());
		menu.setIcon(mappedMenu.getIcon());
		menu.setUsers(mappedMenu.getUsers());
		menu.setCompany(mappedMenu.getCompany());
		menu.setGroups(mappedMenu.getGroups());
		menu.setServices(mappedMenu.getServices());
		menu.setCodes(null);
		menu.setId(mappedMenu.getId());
		menu.setNames(mappedMenu.getNames());
		menu.setDescriptions(mappedMenu.getDescriptions());
		menu.setActive(mappedMenu.isActive());
		menu.setDeleted(mappedMenu.isDeleted());
		return menu;
	}
}