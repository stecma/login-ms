package com.globalia.application.service.menu;

import com.globalia.application.repository.menu.MenuRedis;
import com.globalia.application.service.service.Services;
import com.globalia.application.service.group.Groups;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Map;

@Service
public class Menu extends FindService<MenuItem> {

	@Autowired
	private MenuRedis menuRedis;
	@Autowired
	private Services services;
	@Autowired
	private Groups groups;

	public ItemResponse getItem(final MenuItem item, final Monitor monitor) {
		ItemResponse response = this.menuRedis.getItem(item, monitor);
		if (response.getError() == null) {
			this.completeMenu(response.getMenu(), monitor);
		}
		return response;
	}

	private void completeMenu(final MenuItem menu, final Monitor monitor) {
		if (menu.getCodes() != null && !menu.getCodes().isEmpty()) {
			Map<String, ServiceItem> map = this.services.getAllServices(monitor);
			menu.setServices((LinkedHashSet<ServiceItem>) this.services.addServices(menu.getCodes(), map));
			menu.setGroups((LinkedHashSet<GroupItem>) this.groups.addGroups(menu.getCodes(), this.groups.getAllGroups(monitor), map));
			menu.setCodes(null);
		}
	}
}