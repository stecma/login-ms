package com.globalia.application.service.master;

import com.globalia.application.repository.master.MasterRedis;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service
public class Master extends FindService<MasterItem> {

	@Autowired
	private MasterRedis masterRedis;

	public ItemResponse getItem(final MasterItem item, final Monitor monitor) {
		ItemResponse master = this.masterRedis.getItem(item, monitor);
		if (master.getError() == null) {
			this.completeMaster(master.getMaster(), monitor);
		}
		return master;
	}

	void completeMaster(final MasterItem master, final Monitor monitor) {
		if (master.getCodes() != null) {
			MasterItem filter = new MasterItem();
			for (PairValue pair : master.getCodes()) {
				for (String code : pair.getValue()) {
					if (master.getMasters() == null) {
						master.setMasters(new HashSet<>());
					}
					filter.setEntity(pair.getKey());
					filter.setId(code);
					ItemResponse aux = this.masterRedis.getItem(filter, monitor);
					if (aux.getError() == null) {
						master.getMasters().add(aux.getMaster());
					}
				}
			}
			master.setCodes(null);
		}
	}
}