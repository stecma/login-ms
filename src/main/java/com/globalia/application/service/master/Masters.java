package com.globalia.application.service.master;

import com.globalia.application.repository.master.MastersRedis;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class Masters extends FindAllService<MasterItem> {

	@Autowired
	private MastersRedis masterRedis;
	@Autowired
	private Master master;

	public AllItemsResponse getAllItems(final MasterItem item, final Monitor monitor) {
		AllItemsResponse masters = this.masterRedis.getAllItems(item, monitor);
		if (masters.getError() == null) {
			for (MasterItem aux : masters.getMasters()) {
				this.master.completeMaster(aux, monitor);
			}
		}
		return masters;
	}

	/**
	 * Validate master key.
	 *
	 * @param entity Entity.
	 * @param key    Master key.
	 * @return If the key is valid.
	 */
	public boolean validateKey(final String entity, final String key) {
		Set<String> codes = this.masterRedis.findCodes(entity);
		return codes.contains(key);
	}
}