package com.globalia.application.service.master;

import com.globalia.application.repository.master.SaveMasterRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SaveMaster extends SaveService<MasterItem> {

	@Autowired
	private SaveMasterRedis masterRedis;

	public ItemResponse createItem(final MasterItem item, final Monitor monitor) {
		item.setCreated(true);
		return this.masterRedis.addItem(item, monitor);
	}

	public ItemResponse updateItem(final MasterItem item, final Monitor monitor) {
		return this.masterRedis.addItem(item, monitor);
	}

	public ItemResponse deleteItem(final MasterItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.masterRedis.addItem(item, monitor);
	}
}