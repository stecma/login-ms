package com.globalia.application.service.user;

import com.globalia.application.repository.user.SaveUserRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.UserItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.StatType;
import com.globalia.exception.Error;
import com.globalia.monitoring.Monitor;
import com.globalia.security.AESPassEncrypter;
import com.globalia.service.SaveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;
import org.springframework.util.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
@RefreshScope
public class SaveUser extends SaveService<UserItem> {

	@Value("${jwt.encryptSecret}")
	private String encryptSecret;
	@Value("${jwt.encryptAlgorithm}")
	private String encryptAlgorithm;
	@Value("${logLevel}")
	private LogType logLevel;

	@Autowired
	private SaveUserRedis userRedis;
	@Autowired
	private AESPassEncrypter encrypter;

	public ItemResponse createItem(final UserItem item, final Monitor monitor) {
		ItemResponse response;
		try {
			item.setCreated(true);
			if (StringUtils.hasText(item.getPass())) {
				this.encryptPass(item, monitor);
			}
			response = this.userRedis.addItem(item, monitor);
		} catch (IllegalBlockSizeException | InvalidKeyException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
			response = new ItemResponse();
			response.setError(new Error(e.getMessage(), ErrorLayer.SERVICE_LAYER));
		}
		return response;
	}

	public ItemResponse updateItem(final UserItem item, final Monitor monitor) {
		return this.updateItem(item, false, monitor);
	}

	public ItemResponse deleteItem(final UserItem item, final Monitor monitor) {
		item.setDeleted(true);
		return this.userRedis.addItem(item, monitor);
	}

	/**
	 * Update user.
	 *
	 * @param item        User.
	 * @param encryptPass Indicate if you must encrypt the password.
	 * @param monitor     Monitor.
	 * @return User.
	 */
	public ItemResponse updateItem(final UserItem item, final boolean encryptPass, final Monitor monitor) {
		ItemResponse response;
		try {
			if (encryptPass) {
				this.encryptPass(item, monitor);
			}
			item.setThemes(null);
			response = this.userRedis.addItem(item, monitor);
		} catch (IllegalBlockSizeException | InvalidKeyException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException e) {
			response = new ItemResponse();
			response.setError(new Error(e.getMessage(), ErrorLayer.SERVICE_LAYER));
		}
		return response;
	}

	private void encryptPass(final UserItem item, final Monitor monitor) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		StopWatch watch = new StopWatch();
		watch.start();
		item.setPass(this.encrypter.encrypt(item.getPass(), this.encryptSecret, this.encryptAlgorithm, monitor, this.logLevel));
		watch.stop();
		getHandler().addStat(monitor, StatType.ENCRYPT, watch.getTotalTimeMillis());
	}
}