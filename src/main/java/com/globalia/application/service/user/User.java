package com.globalia.application.service.user;

import com.globalia.Context;
import com.globalia.application.repository.user.UserRedis;
import com.globalia.application.service.service.Services;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.master.Masters;
import com.globalia.application.service.menu.Menus;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.UserItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.dto.login.UserMenuCodes;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.Error;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@Service
@RefreshScope
public class User extends FindService<UserItem> {

	@Value("${themes}")
	private String themes;

	@Autowired
	private UserRedis userRedis;
	@Autowired
	private Services services;
	@Autowired
	private Groups groups;
	@Autowired
	private Menus menus;
	@Autowired
	private Masters masters;
	@Autowired
	private Master master;
	@Autowired
	private Context context;

	public ItemResponse getItem(final UserItem item, final Monitor monitor) {
		ItemResponse user = this.userRedis.getItem(item, monitor);
		if (user.getError() == null) {
			this.completeUser(user.getUser(), monitor);
			this.completeMenu(user.getUser(), monitor);
		}
		return user;
	}

	public ItemResponse getUser(final String userId, final Monitor monitor) {
		UserItem filter = new UserItem();
		filter.setId(userId);

		ItemResponse item = this.getItem(filter, monitor);
		if (item.getError() == null) {
			if (!item.getUser().isActive()) {
				item.setError(new Error("User not exists or is disabled", ErrorLayer.SERVICE_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
				return item;
			}

			boolean found = false;
			String environment = this.context.getEnvironment();
			for (MasterItem env : item.getUser().getEnvironments()) {
				if (!environment.equalsIgnoreCase(env.getId())) {
					continue;
				}
				found = true;
			}

			if (!found) {
				item.setError(new Error("User haven't got access to this environment", ErrorLayer.SERVICE_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
				return item;
			}
			item.getUser().setThemes(this.themes);
		}
		return item;
	}

	private void completeUser(final UserItem user, final Monitor monitor) {
		if (user.getCodes() != null && !user.getCodes().isEmpty()) {
			Set<String> companies = new HashSet<>();
			user.setCompanies(this.getMaster(MasterType.COMPANY.name(), user.getCodes(), companies, monitor));
			user.setEnvironments(this.getMaster(MasterType.ENVIRONMENT.name(), user.getCodes(), companies, monitor));
			user.setCodes(null);
			this.getBusiness(user, companies, monitor);
		}
	}

	private void getBusiness(final UserItem user, final Set<String> companies, final Monitor monitor) {
		MasterItem filter = new MasterItem();
		filter.setEntity(MasterType.BUSINESS_APP.name());
		AllItemsResponse business = this.masters.getAllItems(filter, monitor);

		LinkedHashSet<MasterItem> list = new LinkedHashSet<>();
		if (business.getError() == null) {
			for (MasterItem item : business.getMasters()) {
				filter.setId(item.getId());
				this.addBusiness(item.getMasters(), list, companies, filter, monitor);
			}
		}

		if (!list.isEmpty()) {
			user.setBusiness(list);
		}
	}

	private void addBusiness(final Set<MasterItem> allMasters, final LinkedHashSet<MasterItem> list, final Set<String> companies, final MasterItem filter, final Monitor monitor) {
		for (MasterItem item : allMasters) {
			if (item.getEntity().equalsIgnoreCase(MasterType.COMPANY.name()) && companies.contains(item.getId())) {
				ItemResponse aux = this.master.getItem(filter, monitor);
				if (aux.getError() == null) {
					list.add(aux.getMaster());
				}
			}
		}
	}

	private LinkedHashSet<MasterItem> getMaster(final String masterType, final Set<PairValue> codes, final Set<String> companies, final Monitor monitor) {
		LinkedHashSet<MasterItem> items = new LinkedHashSet<>();
		MasterItem filter = new MasterItem();
		for (PairValue pair : codes) {
			if (masterType.equalsIgnoreCase(pair.getKey())) {
				if (masterType.equalsIgnoreCase(MasterType.COMPANY.name())) {
					companies.addAll(pair.getValue());
				}
				this.addMaster(filter, items, pair.getKey(), pair.getValue(), monitor);
			}
		}
		return (items.isEmpty()) ? null : items;
	}

	private void addMaster(final MasterItem filter, final Set<MasterItem> masters, final String key, final LinkedHashSet<String> codes, final Monitor monitor) {
		for (String code : codes) {
			filter.setEntity(key);
			filter.setId(code);

			ItemResponse aux = this.master.getItem(filter, monitor);
			if (aux.getError() == null) {
				masters.add(aux.getMaster());
			}
		}
	}

	private void completeMenu(final UserItem user, final Monitor monitor) {
		if (user.getConfiguration() != null && !user.getConfiguration().isEmpty()) {
			Map<String, GroupItem> grpMap = this.groups.getAllGroups(monitor);
			Map<String, ServiceItem> srvMap = this.services.getAllServices(monitor);
			Map<String, MenuItem> mnuMap = this.menus.getAllMenus(monitor);

			if (!mnuMap.isEmpty() && !grpMap.isEmpty() && !srvMap.isEmpty()) {
				for (UserMenu config : user.getConfiguration()) {
					if (config.getCodes() != null) {
						config.setMenus(this.addMenu(config.getCodes(), String.format("%s¬%s", config.getEnvironment(), config.getCompany()), mnuMap, grpMap, srvMap));
						config.setCodes(null);
					}
				}
			}
		}
	}

	private LinkedHashSet<MenuItem> addMenu(final LinkedHashSet<UserMenuCodes> codes, final String key, final Map<String, MenuItem> mnuMap, final Map<String, GroupItem> grpMap, final Map<String, ServiceItem> srvMap) {
		LinkedHashSet<MenuItem> values = new LinkedHashSet<>();
		Map<String, MenuItem> menusMap = new HashMap<>();
		Map<String, GroupItem> groupsMap = new HashMap<>();
		for (UserMenuCodes code : codes) {
			MenuItem menu = this.addMenus(values, menusMap, key, code.getMenu(), mnuMap);
			if (StringUtils.hasText(code.getGroup())) {
				GroupItem group = this.addGroups(menu, groupsMap, code.getGroup(), grpMap);
				if (group.getServices() == null) {
					group.setServices(new LinkedHashSet<>());
				}
				group.getServices().add(this.addServices(code.getService(), code.isReadOnly(), srvMap));
			} else {
				if (menu.getServices() == null) {
					menu.setServices(new LinkedHashSet<>());
				}
				menu.getServices().add(this.addServices(code.getService(), code.isReadOnly(), srvMap));
			}
		}
		return values;
	}

	private GroupItem addGroups(final MenuItem menu, final Map<String, GroupItem> groups, final String groupId, final Map<String, GroupItem> grpMap) {
		if (!groups.containsKey(groupId)) {
			GroupItem aux = this.groups.createNewGroup(grpMap.get(groupId));
			aux.setServices(null);
			groups.put(groupId, aux);
			if (menu.getGroups() == null) {
				menu.setGroups(new LinkedHashSet<>());
			}
			menu.getGroups().add(aux);
		}
		return groups.get(groupId);
	}

	private ServiceItem addServices(final String serviceId, final boolean readOnly, final Map<String, ServiceItem> srvMap) {
		ServiceItem service = srvMap.get(serviceId);
		if (service != null && service.getUrl() != null) {
			service.getUrl().setReadOnly(readOnly);
		}
		return service;
	}

	private MenuItem addMenus(final LinkedHashSet<MenuItem> values, final Map<String, MenuItem> menus, final String code, final String menuId, final Map<String, MenuItem> mnuMap) {
		String key = String.format("%s¬%s", code, menuId);
		if (!menus.containsKey(key)) {
			MenuItem aux = this.menus.createNewMenu(mnuMap.get(menuId));
			aux.setGroups(null);
			aux.setServices(null);
			menus.put(key, aux);
			values.add(aux);
		}
		return menus.get(key);
	}
}