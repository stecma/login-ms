package com.globalia.application.service.user;

import com.globalia.application.repository.user.UsersRedis;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.UserItem;
import com.globalia.monitoring.Monitor;
import com.globalia.service.FindAllService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Users extends FindAllService<UserItem> {

	@Autowired
	private UsersRedis userRedis;

	public AllItemsResponse getAllItems(final UserItem item, final Monitor monitor) {
		return this.userRedis.getAllItems(item, monitor);
	}
}