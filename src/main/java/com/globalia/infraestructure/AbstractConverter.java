package com.globalia.infraestructure;

import com.globalia.api.login.ApiRQ;
import com.globalia.dto.login.ItemResponse;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@RefreshScope
public abstract class AbstractConverter {

	protected Object getRequest(final ItemConverter.apiRequest entity, final ApiRQ request) {
		switch (entity) {
			case USER:
				return request.getUser();
			case MENU:
				return request.getMenu();
			case GROUP:
				return request.getGroup();
			case SERVICE:
				return request.getService();
			default:
				return request.getMaster();
		}
	}

	protected Object getResponse(final ItemConverter.apiRequest entity, final ItemResponse response) {
		switch (entity) {
			case MENU:
				return response.getMenu();
			case GROUP:
				return response.getGroup();
			case SERVICE:
				return response.getService();
			case USER:
				return response.getUser();
			default:
				return response.getMaster();
		}
	}
}