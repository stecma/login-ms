package com.globalia.infraestructure.aspect;

import com.globalia.api.login.ApiRQ;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractAspect;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Aspect
public class LoginAspect extends AbstractAspect {

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && (execution(* com.globalia.infraestructure.rest.user.Validate.*(..)) || execution(* com.globalia.infraestructure.rest.user.Refresh.*(..)) || execution(* com.globalia.infraestructure.rest.user.Reset.*(..))) && args(token, request)", argNames = "point,token,request")
	public Object loginControllerToken(final ProceedingJoinPoint point, final String token, final ApiRQ request) throws Throwable {
		if (request == null || !StringUtils.hasText(token)) {
			throw new ValidateException(new Error("Invalid request", ErrorLayer.API_LAYER), null);
		}
		return execute(point, request.getMonitor());
	}

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.user.Authenticate.*(..)) && args(request)", argNames = "point,request")
	public Object loginController(final ProceedingJoinPoint point, final ApiRQ request) throws Throwable {
		if (request == null || request.getLogin() == null) {
			throw new ValidateException(new Error("Invalid request", ErrorLayer.API_LAYER), null);
		}
		this.loginRQValidation(request);
		return execute(point, request.getMonitor());
	}

	private void loginRQValidation(final ApiRQ request) {
		if (!StringUtils.hasText(request.getLogin().getUser())) {
			throw new ValidateException(new Error("Username is mandatory", ErrorLayer.API_LAYER), request.getMonitor());
		}
		if (!StringUtils.hasText(request.getLogin().getPass())) {
			throw new ValidateException(new Error("Password is mandatory", ErrorLayer.API_LAYER), request.getMonitor());
		}
	}
}
