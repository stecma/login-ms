package com.globalia.infraestructure.aspect;

import com.globalia.enumeration.LogType;
import com.globalia.enumeration.StatType;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
@RefreshScope
public class CoreAspect {

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;
	@Value("${logLevel}")
	private LogType logLevel;

	@Autowired
	private MonitorHandler handler;

	@Around(value = "within(@org.springframework.stereotype.Repository *) && args(.., monitor)", argNames = "point,monitor")
	public Object loggerRedis(final ProceedingJoinPoint point, final Monitor monitor) throws Throwable {
		StatType stat = StatType.REDIS_GET;
		if (point.getSignature().getName().equals("addItem")) {
			stat = StatType.REDIS_ADD;
		}
		return this.execute(point, monitor, stat, "Start Redis Access", "End Redis Access");
	}

	@Around(value = "within(@org.springframework.stereotype.Service *) && args(.., monitor)", argNames = "point,monitor")
	public Object loggerService(final ProceedingJoinPoint point, final Monitor monitor) throws Throwable {
		return this.execute(point, monitor, null, "Start Service", "End Service");
	}

	private Object execute(final ProceedingJoinPoint point, final Monitor monitor, final StatType statType, final String iniMsg, final String endMsg) throws Throwable {
		StopWatch watch = new StopWatch();
		this.addLog(monitor, point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), iniMsg);
		watch.start();
		Object result = point.proceed();
		watch.stop();
		if (statType != null) {
			this.handler.addStat(monitor, statType, watch.getTotalTimeMillis());
		}
		this.addLog(monitor, point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), endMsg);
		return result;
	}

	private void addLog(final Monitor monitor, final String className, final String methodName, final String message) {
		if (monitor != null) {
			monitor.setService(this.appName);
			monitor.setPort(this.port);
			this.handler.addDebugLog(monitor, className, methodName, 0, message, this.logLevel);
		}
	}
}