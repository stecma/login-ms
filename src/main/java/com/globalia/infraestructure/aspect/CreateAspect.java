package com.globalia.infraestructure.aspect;

import com.globalia.api.login.ApiRQ;
import com.globalia.infraestructure.AbstractAspect;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class CreateAspect extends AbstractAspect {

	@Around(value = "within(@org.springframework.web.bind.annotation.RestController *) && execution(* com.globalia.infraestructure.rest.Create.*(..)) && args(entity,request)", argNames = "point,entity,request")
	public Object createController(final ProceedingJoinPoint point, final String entity, final ApiRQ request) throws Throwable {
		ItemConverter.apiRequest apiRequest = ItemConverter.apiRequest.valueOf(entity.toUpperCase());
		validateRequest(apiRequest, request);
		contentValidation(apiRequest, request, true);
		return execute(point, request.getMonitor());
	}
}
