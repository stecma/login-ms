package com.globalia.infraestructure.rest.user;

import com.globalia.api.ApiRS;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.user.User;
import com.globalia.aspect.Login;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractController;
import com.globalia.security.AESPassEncrypter;
import com.globalia.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Login
@RefreshScope
@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
class Validate extends AbstractController {

	private static final String AUTHORIZATION = "Authorization";

	@Value("${jwt.encryptSecret}")
	private String encryptSecret;
	@Value("${jwt.encryptAlgorithm}")
	private String encryptAlgorithm;

	@Autowired
	private User user;
	@Autowired
	private AESPassEncrypter encrypter;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@PostMapping(value = "/validate")
	public ResponseEntity<ApiRS> validateToken(@RequestHeader(value = AUTHORIZATION) final String token, @RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse(token, request), HttpStatus.OK, token, true, request.getMonitor());
	}

	protected Object getResponse(final String token, final ApiRQ request) {
		ItemResponse item;
		try {
			item = this.user.getUser(this.jwtTokenProvider.getUser(token, getTokenConfig(), request.getMonitor()), request.getMonitor());
			if (item.getError() == null) {
				item.getUser().setPass(this.encrypter.decrypt(item.getUser().getPass(), this.encryptSecret, this.encryptAlgorithm, request.getMonitor(), getLogLevel()));
			}
		} catch (ValidateException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
			item = new ItemResponse();
			item.setError(new Error(e.getMessage(), ErrorLayer.API_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
		}
		return item.getError() != null ? item.getError() : item.getUser();
	}
}
