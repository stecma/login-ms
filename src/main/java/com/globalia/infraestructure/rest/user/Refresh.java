package com.globalia.infraestructure.rest.user;

import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.user.User;
import com.globalia.aspect.Login;
import com.globalia.dto.login.ItemResponse;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractController;
import com.globalia.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Login
@RefreshScope
@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
class Refresh extends AbstractController {

	@Autowired
	private User user;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@PostMapping(value = "/refresh")
	public ResponseEntity<Void> refreshToken(@RequestHeader(value = AUTHORIZATION) final String token, @RequestBody final ApiRQ request) {
		String authToken = (String) this.getResponse(token, request);
		return new ResponseEntity<>(headerBuilder(authToken, true), StringUtils.hasText(authToken) ? HttpStatus.OK : HttpStatus.UNAUTHORIZED);
	}

	protected Object getResponse(final String token, final ApiRQ request) {
		try {
			ItemResponse item = this.user.getUser(this.jwtTokenProvider.getUser(token, getTokenConfig(), request.getMonitor()), request.getMonitor());
			if (item.getError() == null) {
				return this.jwtTokenProvider.refreshToken(token, item.getUser(), getTokenConfig(), request.getMonitor());
			}
		} catch (ValidateException ignored) {
			//
		}
		return null;
	}
}
