package com.globalia.infraestructure.rest.user;

import com.globalia.api.ApiRS;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.user.SaveUser;
import com.globalia.application.service.user.User;
import com.globalia.aspect.Login;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractController;
import com.globalia.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Login
@RefreshScope
@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
class Reset extends AbstractController {

	@Autowired
	private User user;
	@Autowired
	private SaveUser save;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@PostMapping(value = "/reset")
	public ResponseEntity<ApiRS> reset(@RequestHeader(value = AUTHORIZATION) final String token, @RequestBody final ApiRQ request) {
		return buildResponse(this.getResponse(token, request), HttpStatus.RESET_CONTENT, null, false, request.getMonitor());
	}

	protected Object getResponse(final String token, final ApiRQ request) {
		ItemResponse item;
		try {
			item = this.user.getUser(this.jwtTokenProvider.getUser(token, getTokenConfig(), request.getMonitor()), request.getMonitor());
			if (item.getError() == null) {
				item.getUser().setPass("");
				item = this.save.updateItem(item.getUser(), false, request.getMonitor());
			}
		} catch (ValidateException ex) {
			item = new ItemResponse();
			item.setError(new Error(ex.getMessage(), ErrorLayer.SERVICE_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value()));
		}

		if (item.getError() != null) {
			return item.getError();
		}
		return item.getUser();
	}
}
