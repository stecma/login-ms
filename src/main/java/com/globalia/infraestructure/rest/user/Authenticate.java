package com.globalia.infraestructure.rest.user;

import com.globalia.api.ApiRS;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.user.SaveUser;
import com.globalia.application.service.user.User;
import com.globalia.aspect.Login;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.UserItem;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.AbstractController;
import com.globalia.security.AESPassEncrypter;
import com.globalia.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Login
@RefreshScope
@RestController
@RequestMapping(value = "/api/v1", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
class Authenticate extends AbstractController {

	@Value("${jwt.encryptSecret}")
	private String encryptSecret;
	@Value("${jwt.encryptAlgorithm}")
	private String encryptAlgorithm;

	@Autowired
	private User user;
	@Autowired
	private SaveUser save;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private AESPassEncrypter encrypter;
	@Autowired
	private AuthenticationManager authenticationManager;

	@PostMapping(value = "/login")
	public ResponseEntity<ApiRS> login(@RequestBody final ApiRQ request) {
		this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getLogin().getUser(), request.getLogin().getPass()));

		Object obj = this.getResponse(request);
		String token = null;
		if (obj instanceof UserItem) {
			token = ((UserItem) obj).getToken();
		}
		return buildResponse(obj, HttpStatus.CREATED, token, true, request.getMonitor());
	}

	protected Object getResponse(final ApiRQ request) {
		ItemResponse item = this.user.getUser(request.getLogin().getUser(), request.getMonitor());
		if (item.getError() != null) {
			return item.getError();
		}

		try {
			String pass = this.encrypter.encrypt(request.getLogin().getPass(), this.encryptSecret, this.encryptAlgorithm, request.getMonitor(), getLogLevel());
			if (!StringUtils.hasText(item.getUser().getPass()) || !item.getUser().getPass().equals(pass)) {
				item.getUser().setPass(request.getLogin().getPass());
				item = this.save.updateItem(item.getUser(), true, request.getMonitor());
				if (item.getError() != null) {
					return item.getError();
				}
			}

			item.getUser().setToken(this.jwtTokenProvider.createToken(item.getUser(), this.getTokenConfig(), request.getMonitor()));
		} catch (ValidateException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException ex) {
			return new Error(ex.getMessage(), ErrorLayer.SERVICE_LAYER, HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return item.getUser();
	}
}
