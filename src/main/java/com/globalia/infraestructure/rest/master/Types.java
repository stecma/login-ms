package com.globalia.infraestructure.rest.master;

import com.globalia.api.ApiRS;
import com.globalia.enumeration.login.MasterType;
import com.globalia.infraestructure.AbstractController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/master", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class Types extends AbstractController {

	/**
	 * Get master type.
	 *
	 * @return Master types.
	 */
	@GetMapping(value = "")
	public ResponseEntity<ApiRS> getMasterType() {
		return buildResponse(MasterType.values(), HttpStatus.OK, null);
	}
}
