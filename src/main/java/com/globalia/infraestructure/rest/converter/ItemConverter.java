package com.globalia.infraestructure.rest.converter;

import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.group.Group;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.menu.Menu;
import com.globalia.application.service.service.Service;
import com.globalia.application.service.user.User;
import com.globalia.dto.login.ItemResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.IFind;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class ItemConverter extends AbstractConverter {

	@Autowired
	private User user;
	@Autowired
	private Menu menu;
	@Autowired
	private Service service;
	@Autowired
	private Group group;
	@Autowired
	private Master master;

	public Object getItem(final apiRequest entity, final ApiRQ request) {
		ItemResponse response = (ItemResponse) this.getFind(entity).getItem(getRequest(entity, request), request.getMonitor());
		return response.getError() != null ? response.getError() : getResponse(entity, response);
	}

	private IFind getFind(final apiRequest entity) {
		switch (entity) {
			case GROUP:
				return this.group;
			case SERVICE:
				return this.service;
			case USER:
				return this.user;
			case MENU:
				return this.menu;
			case MASTER:
			default:
				return this.master;
		}
	}

	public enum apiRequest {
		USER,
		MENU,
		GROUP,
		SERVICE,
		MASTER
	}
}