package com.globalia.infraestructure.rest.converter;

import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.master.Masters;
import com.globalia.application.service.menu.Menus;
import com.globalia.application.service.service.Services;
import com.globalia.application.service.user.Users;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.IFindAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class AllItemsConverter extends AbstractConverter {

	@Autowired
	private Services services;
	@Autowired
	private Users users;
	@Autowired
	private Menus menus;
	@Autowired
	private Groups groups;
	@Autowired
	private Masters masters;

	public Object getAllItems(final ItemConverter.apiRequest entity, final ApiRQ request) {
		AllItemsResponse response = (AllItemsResponse) this.getFindAll(entity).getAllItems(getRequest(entity, request), request.getMonitor());
		return response.getError() != null ? response.getError() : this.getResponse(entity, response);
	}

	private Object getResponse(final ItemConverter.apiRequest entity, final AllItemsResponse response) {
		switch (entity) {
			case USER:
				return response.getUsers();
			case MENU:
				return response.getMenus();
			case GROUP:
				return response.getGroups();
			case SERVICE:
				return response.getServices();
			default:
				return response.getMasters();
		}
	}

	private IFindAll getFindAll(final ItemConverter.apiRequest entity) {
		switch (entity) {
			case USER:
				return this.users;
			case MENU:
				return this.menus;
			case GROUP:
				return this.groups;
			case SERVICE:
				return this.services;
			case MASTER:
			default:
				return this.masters;
		}
	}
}