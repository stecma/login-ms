package com.globalia.infraestructure.rest.converter;

import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.group.SaveGroup;
import com.globalia.application.service.master.SaveMaster;
import com.globalia.application.service.menu.SaveMenu;
import com.globalia.application.service.user.SaveUser;
import com.globalia.application.service.service.ServiceSave;
import com.globalia.dto.login.ItemResponse;
import com.globalia.infraestructure.AbstractConverter;
import com.globalia.service.ISave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings("unchecked")
@Component
public class SaveConverter extends AbstractConverter {

	@Autowired
	private SaveUser saveUser;
	@Autowired
	private SaveMenu saveMenu;
	@Autowired
	private ServiceSave saveService;
	@Autowired
	private SaveGroup saveGroup;
	@Autowired
	private SaveMaster saveMaster;

	public Object save(final ItemConverter.apiRequest entity, final saveAction action, final ApiRQ request) {
		ItemResponse response;
		switch (action) {
			case CREATE:
				response = (ItemResponse) this.getSave(entity).createItem(getRequest(entity, request), request.getMonitor());
				break;
			case UPDATE:
				response = (ItemResponse) this.getSave(entity).updateItem(getRequest(entity, request), request.getMonitor());
				break;
			default:
				response = (ItemResponse) this.getSave(entity).deleteItem(getRequest(entity, request), request.getMonitor());
		}

		return response.getError() != null ? response.getError() : getResponse(entity, response);
	}

	private ISave getSave(final ItemConverter.apiRequest entity) {
		switch (entity) {
			case SERVICE:
				return this.saveService;
			case USER:
				return this.saveUser;
			case MENU:
				return this.saveMenu;
			case GROUP:
				return this.saveGroup;
			case MASTER:
			default:
				return this.saveMaster;
		}
	}

	public enum saveAction {
		CREATE,
		UPDATE,
		DELETE
	}
}