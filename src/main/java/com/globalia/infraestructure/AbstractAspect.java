package com.globalia.infraestructure;

import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.I18n;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.UserItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.infraestructure.rest.converter.ItemConverter;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.util.StringUtils;

import java.util.LinkedHashSet;
import java.util.Set;

@RefreshScope
public abstract class AbstractAspect {

	protected static final String ID_MANDATORY = "Field Id is mandatory";

	@Autowired
	private MonitorHandler handler;
	@Autowired
	private Masters masters;

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;
	@Value("${logLevel}")
	private LogType logLevel;

	protected Object execute(final ProceedingJoinPoint point, final Monitor monitor) throws Throwable {
		this.addLog(monitor, point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), "Start controller");
		Object result = point.proceed();
		this.addLog(monitor, point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), "End controller");
		return result;
	}

	protected void contentValidation(final ItemConverter.apiRequest entity, final ApiRQ request) {
		this.contentValidation(entity, request, false);
	}

	protected void contentValidation(final ItemConverter.apiRequest entity, final ApiRQ request, final boolean isNew) {
		if (entity == ItemConverter.apiRequest.USER) {
			this.userRQValidation(request, isNew);
		}
		this.menuContentValidation(entity, request, true);
		this.groupContentValidation(entity, request, true);
		this.serviceContentValidation(entity, request, true);
		this.masterContentValidation(entity, request, true);
	}

	private void menuContentValidation(final ItemConverter.apiRequest entity, final ApiRQ request, final boolean all) {
		if (entity == ItemConverter.apiRequest.MENU) {
			if (StringUtils.hasText(request.getMenu().getEnvironment())) {
				masterValidation(MasterType.ENVIRONMENT.name(), request.getMenu().getEnvironment(), request.getMonitor());
			}
			if (StringUtils.hasText(request.getMenu().getCompany())) {
				masterValidation(MasterType.COMPANY.name(), request.getMenu().getCompany(), request.getMonitor());
			}
			if (all) {
				this.i18nValidation(request.getMenu().getNames(), request.getMenu().getDescriptions(), request.getMonitor());
			}
		}
	}

	private void groupContentValidation(final ItemConverter.apiRequest entity, final ApiRQ request, final boolean all) {
		if (entity == ItemConverter.apiRequest.GROUP) {
			if (StringUtils.hasText(request.getGroup().getEnvironment())) {
				this.masterValidation(MasterType.ENVIRONMENT.name(), request.getGroup().getEnvironment(), request.getMonitor());
			}

			if (all) {
				this.i18nValidation(request.getGroup().getNames(), request.getGroup().getDescriptions(), request.getMonitor());
			}
		}
	}

	private void serviceContentValidation(final ItemConverter.apiRequest entity, final ApiRQ request, final boolean all) {
		if (entity == ItemConverter.apiRequest.SERVICE) {
			if (StringUtils.hasText(request.getService().getEnvironment())) {
				this.masterValidation(MasterType.ENVIRONMENT.name(), request.getService().getEnvironment(), request.getMonitor());
			}

			if (all) {
				if (request.getService().getUrl() == null || !StringUtils.hasText(request.getService().getUrl().getRoute())) {
					throw new ValidateException(new Error("Url services is mandatory", ErrorLayer.API_LAYER), request.getMonitor());
				}
				this.i18nValidation(request.getService().getNames(), request.getService().getDescriptions(), request.getMonitor());
			}
		}
	}

	private void masterContentValidation(final ItemConverter.apiRequest entity, final ApiRQ request, final boolean all) {
		if (entity == ItemConverter.apiRequest.MASTER) {
			if (!StringUtils.hasText(request.getMaster().getEntity())) {
				throw new ValidateException(new Error("Entity is mandatory", ErrorLayer.API_LAYER), null);
			}

			if (all) {
				this.i18nValidation(request.getMaster().getNames(), null, request.getMonitor());
			}
		}
	}

	protected void validateRequest(final ItemConverter.apiRequest entity, final ApiRQ request) {
		if (request == null || !isValidRQ(entity, request)) {
			throw new ValidateException(new Error("Invalid request", ErrorLayer.API_LAYER), null);
		}
	}

	protected boolean isEmptyID(final ItemConverter.apiRequest entity, final ApiRQ request) {
		return this.isEmptyIDUser(entity, request) || this.isEmptyIDMenu(entity, request) || this.isEmptyIDGroup(entity, request) || this.isEmptyIDService(entity, request) || this.isEmptyIDMaster(entity, request);
	}

	private boolean isEmptyIDUser(final ItemConverter.apiRequest entity, final ApiRQ request) {
		return entity == ItemConverter.apiRequest.USER && !StringUtils.hasText(request.getUser().getId());
	}

	private boolean isEmptyIDMenu(final ItemConverter.apiRequest entity, final ApiRQ request) {
		return entity == ItemConverter.apiRequest.MENU && !StringUtils.hasText(request.getMenu().getId());
	}

	private boolean isEmptyIDMaster(final ItemConverter.apiRequest entity, final ApiRQ request) {
		return entity == ItemConverter.apiRequest.MASTER && !StringUtils.hasText(request.getMaster().getId());
	}

	private boolean isEmptyIDService(final ItemConverter.apiRequest entity, final ApiRQ request) {
		return entity == ItemConverter.apiRequest.SERVICE && !StringUtils.hasText(request.getService().getId());
	}

	private boolean isEmptyIDGroup(final ItemConverter.apiRequest entity, final ApiRQ request) {
		return entity == ItemConverter.apiRequest.GROUP && !StringUtils.hasText(request.getGroup().getId());
	}

	protected void othersValidation(final ItemConverter.apiRequest entity, final ApiRQ request, final boolean check) {
		this.groupContentValidation(entity, request, false);
		this.serviceContentValidation(entity, request, false);
		this.menuContentValidation(entity, request, false);
		if (check) {
			this.masterContentValidation(entity, request, false);
		}
	}

	protected boolean isValidRQ(final ItemConverter.apiRequest entity, final ApiRQ request) {
		return this.isValidUserRQ(entity, request.getUser()) || this.isValidMenuRQ(entity, request.getMenu()) || this.isValidGroupRQ(entity, request.getGroup()) || this.isValidServiceRQ(entity, request.getService()) || this.isValidMasterRQ(entity, request.getMaster());
	}

	private boolean isValidUserRQ(final ItemConverter.apiRequest entity, final UserItem item) {
		return entity == ItemConverter.apiRequest.USER && item != null;
	}

	private boolean isValidMenuRQ(final ItemConverter.apiRequest entity, final MenuItem item) {
		return entity == ItemConverter.apiRequest.MENU && item != null;
	}

	private boolean isValidMasterRQ(final ItemConverter.apiRequest entity, final MasterItem item) {
		return entity == ItemConverter.apiRequest.MASTER && item != null;
	}

	private boolean isValidServiceRQ(final ItemConverter.apiRequest entity, final ServiceItem item) {
		return entity == ItemConverter.apiRequest.SERVICE && item != null;
	}

	private boolean isValidGroupRQ(final ItemConverter.apiRequest entity, final GroupItem item) {
		return entity == ItemConverter.apiRequest.GROUP && item != null;
	}

	private void masterValidation(final String masterType, final String masterKey, final Monitor monitor) {
		if (!this.masters.validateKey(masterType, masterKey)) {
			throw new ValidateException(new Error(String.format("Cannot find master with id: %s", masterKey), ErrorLayer.API_LAYER), monitor);
		}
	}

	private void i18nValidation(final LinkedHashSet<I18n> names, final LinkedHashSet<I18n> descriptions, final Monitor monitor) {
		if (names == null || names.isEmpty()) {
			throw new ValidateException(new Error("Name is mandatory", ErrorLayer.API_LAYER), monitor);
		}

		for (I18n name : names) {
			this.masterValidation(MasterType.LANGUAGES_APP.name(), name.getKey(), monitor);
		}

		if (descriptions != null && !descriptions.isEmpty()) {
			for (I18n desc : descriptions) {
				this.masterValidation(MasterType.LANGUAGES_APP.name(), desc.getKey(), monitor);
			}
		}
	}

	private void addLog(final Monitor monitor, final String controller, final String methodName, final String message) {
		if (monitor != null) {
			monitor.setService(this.appName);
			monitor.setPort(this.port);
			this.handler.addDebugLog(monitor, controller, methodName, 0, message, this.logLevel);
		}
	}

	private void userRQValidation(final ApiRQ request, final boolean isNew) {
		if (this.isInvalid(request.getUser())) {
			throw new ValidateException(new Error("User/Name/Surname/language/Mail are mandatory", ErrorLayer.API_LAYER), null);
		}
		this.masterValidation(MasterType.LANGUAGES_APP.name(), request.getUser().getLanguage(), request.getMonitor());
		this.companyValidation(request.getUser().getCompanies(), request.getMonitor());
		this.environmentValidation(request.getUser().getEnvironments(), request.getMonitor());
		if (!isNew) {
			this.configValidation(request.getUser(), request.getMonitor());
		}
	}

	private boolean isInvalid(final UserItem user) {
		boolean result = !StringUtils.hasText(user.getId());

		if (!result && (!StringUtils.hasText(user.getName()) || !StringUtils.hasText(user.getSurname()))) {
			result = true;
		}

		if (!result && (!StringUtils.hasText(user.getLanguage()) || !StringUtils.hasText(user.getMail()))) {
			result = true;
		}

		return result;
	}

	private void environmentValidation(final Set<MasterItem> environments, final Monitor monitor) {
		if (environments == null || environments.isEmpty()) {
			throw new ValidateException(new Error("Indicate at least an environment", ErrorLayer.API_LAYER), monitor);
		}

		for (MasterItem env : environments) {
			this.masterValidation(MasterType.ENVIRONMENT.name(), env.getId(), monitor);
		}
	}

	private void companyValidation(final Set<MasterItem> companies, final Monitor monitor) {
		if (companies == null || companies.isEmpty()) {
			throw new ValidateException(new Error("Indicate at least an company", ErrorLayer.API_LAYER), monitor);
		}

		for (MasterItem cia : companies) {
			this.masterValidation(MasterType.COMPANY.name(), cia.getId(), monitor);
		}
	}

	private void configValidation(final UserItem item, final Monitor monitor) {
		if (item.getConfiguration() == null || item.getConfiguration().isEmpty()) {
			throw new ValidateException(new Error("Indicate at least a menu configuration", ErrorLayer.API_LAYER), monitor);
		}

		for (UserMenu menu : item.getConfiguration()) {
			this.menuValidation(menu, item, monitor);
		}
	}

	private void menuValidation(final UserMenu menu, final UserItem item, final Monitor monitor) {
		boolean find = false;
		for (MasterItem env : item.getEnvironments()) {
			if (env.getId().equals(menu.getEnvironment())) {
				find = true;
			}
		}

		if (!find) {
			throw new ValidateException(new Error(String.format("User hasn't access to environment %s", menu.getEnvironment()), ErrorLayer.API_LAYER), monitor);
		}

		find = false;
		for (MasterItem cia : item.getCompanies()) {
			if (cia.getId().equals(menu.getCompany())) {
				find = true;
			}
		}

		if (!find) {
			throw new ValidateException(new Error(String.format("User hasn't access to company %s", menu.getCompany()), ErrorLayer.API_LAYER), monitor);
		}
	}
}