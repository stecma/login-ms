package com.globalia.infraestructure;

import com.globalia.api.ApiRS;
import com.globalia.configuration.TokenConfig;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.monitoring.Monitor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@RefreshScope
public abstract class AbstractController {

	protected static final String AUTHORIZATION = "Authorization";

	@Value("${jwt.secret}")
	private String jwtSecret;
	@Value("${jwt.expirationInMs}")
	private int jwtExpirationInMs;
	@Value("${jwt.expirationAdminInMs}")
	private int jwtExpirationAdminInMs;
	@Value("${jwt.forcedUrl}")
	private String forcedUrl;
	@Getter
	@Value("${logLevel}")
	private LogType logLevel;

	protected ResponseEntity<ApiRS> buildResponse(final Object obj, final HttpStatus status, final Monitor monitor) {
		return this.buildResponse(obj, status, null, false, monitor);
	}

	protected ResponseEntity<ApiRS> buildResponse(final Object obj, final HttpStatus status, final String token, final boolean addToken, final Monitor monitor) {
		HttpStatus statusRS = status;
		if (obj instanceof Error) {
			statusRS = HttpStatus.valueOf(((Error) obj).getStatus());
			if (statusRS == HttpStatus.NO_CONTENT) {
				if (addToken) {
					statusRS = HttpStatus.UNAUTHORIZED;
				} else {
					statusRS = HttpStatus.OK;
				}
			}
		}

		ApiRS response = new ApiRS();
		response.setMonitor(monitor);
		response.setResponse(obj);
		response.setStatus(statusRS.value());
		return new ResponseEntity<>(response, this.headerBuilder(token, addToken), statusRS);
	}

	protected TokenConfig getTokenConfig() {
		TokenConfig tokenConfig = new TokenConfig();
		tokenConfig.setSecret(Base64.getEncoder().encodeToString(this.jwtSecret.getBytes()));
		tokenConfig.setExpirationAdminInMs(this.jwtExpirationAdminInMs);
		tokenConfig.setExpirationInMs(this.jwtExpirationInMs);
		tokenConfig.setForcedUrl(this.forcedUrl);
		tokenConfig.setAllowedLogLevel(this.logLevel);
		return tokenConfig;
	}

	public HttpHeaders headerBuilder(final String token, final boolean addToken) {
		HttpHeaders headers = new HttpHeaders();
		List<String> headerlist = new ArrayList<>();
		List<String> exposeList = new ArrayList<>();
		headerlist.add("Content-Type");
		headerlist.add(" Accept");
		headerlist.add("X-Requested-With");
		if (addToken && StringUtils.hasText(token)) {
			headerlist.add(AbstractController.AUTHORIZATION);
			headers.setAccessControlAllowHeaders(headerlist);
			exposeList.add(AbstractController.AUTHORIZATION);
			headers.setAccessControlExposeHeaders(exposeList);
			headers.set(AbstractController.AUTHORIZATION, token);
		}
		return headers;
	}
}