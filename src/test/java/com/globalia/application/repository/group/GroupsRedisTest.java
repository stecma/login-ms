package com.globalia.application.repository.group;

import com.globalia.HelperTest;
import com.globalia.dto.login.GroupItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupsRedisTest extends HelperTest {

	@InjectMocks
	private GroupsRedis groups;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(GroupsRedis.class, this.groups, "groupsKey");
	}

	@Test
	public void testGetAllGroupRuntimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.groups.getAllItems(group(), monitor()).getError());
	}

	@Test
	public void testGetAllGroupIOException() throws IOException {
		GroupItem group = group();

		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", GroupItem.class)).thenThrow(IOException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.groups.getAllItems(group, monitor()).getError());
	}

	@Test
	public void testGetAllGroupIsDeleted() throws IOException {
		GroupItem group = group();
		group.setDeleted(true);

		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", GroupItem.class)).thenReturn(group);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.groups.getAllItems(group, monitor()).getError());
	}

	@Test
	public void testGetAllGroup() throws IOException {
		GroupItem group = group();

		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");
		map.put("2", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", GroupItem.class)).thenReturn(group);
		assertNull(this.groups.getAllItems(group, monitor()).getError());
	}
}
