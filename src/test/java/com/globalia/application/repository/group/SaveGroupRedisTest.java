package com.globalia.application.repository.group;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.login.GroupItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashSet;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveGroupRedisTest extends HelperTest {

	@InjectMocks
	private SaveGroupRedis save;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(SaveGroupRedis.class, this.save, "groupsKey");
	}

	@Test
	public void testAddItemJsonProcessingException() throws JsonProcessingException {
		GroupItem group = group();
		group.setId(null);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.addItem(group, monitor()).getError());
	}

	@Test
	public void testAddItemRuntimeException() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		doThrow(RuntimeException.class).when(this.client).add(nullable(String.class), anyString(), anyString());
		when(this.redisAccess.getRediskey(anyString())).thenReturn("key");
		assertNotNull(this.save.addItem(group(), monitor()).getError());
	}

	@Test
	public void testAddItemDeleted() throws JsonProcessingException {
		GroupItem group = group();
		group.setDeleted(true);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(group, monitor()).getError());
	}

	@Test
	public void testAddItemNoServices() throws JsonProcessingException {
		GroupItem group = group();
		group.setServices(null);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(group, monitor()).getError());
	}

	@Test
	public void testAddItemEmptyServices() throws JsonProcessingException {
		GroupItem group = group();
		group.setServices(new LinkedHashSet<>());
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(group, monitor()).getError());
	}

	@Test
	public void testAddItem() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(group(), monitor()).getError());
	}
}
