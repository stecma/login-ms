package com.globalia.application.repository.group;

import com.globalia.HelperTest;
import com.globalia.dto.login.GroupItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupRedisTest extends HelperTest {

	@InjectMocks
	private GroupRedis group;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(GroupRedis.class, this.group, "groupsKey");
	}

	@Test
	public void testGetGroupNoJson() {
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.group.getItem(group(), monitor()).getError());
	}

	@Test
	public void testGetGroupIOException() throws IOException {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.group.getItem(group(), monitor()).getError());
	}

	@Test
	public void testGetGroupRunTimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.group.getItem(group(), monitor()).getError());
	}

	@Test
	public void testGetGroupJsonNull() throws IOException {
		when(this.client.get(nullable(String.class), anyString())).thenReturn(null);
		when(this.jsonHandler.fromJson(nullable(String.class), (Class<?>) any())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.group.getItem(group(), monitor()).getError());
	}

	@Test
	public void testGetGroupIsDelete() throws IOException {
		GroupItem group = group();
		group.setDeleted(true);

		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.group.getItem(group(), monitor()).getError());
	}

	@Test
	public void testGetGroup() throws IOException {
		GroupItem group = group();

		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(group);
		assertNull(this.group.getItem(group, monitor()).getError());
	}
}
