package com.globalia.application.repository.service;

import com.globalia.HelperTest;
import com.globalia.dto.login.ServiceItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ServicesRedisTest extends HelperTest {

	@InjectMocks
	private ServicesRedis services;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(ServicesRedis.class, this.services, "servicesKey");
	}

	@Test
	public void testGetAllMasterRuntimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.services.getAllItems(service(), monitor()).getError());
	}

	@Test
	public void testGetAllMasterIOException() throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", ServiceItem.class)).thenThrow(IOException.class);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.services.getAllItems(service(), monitor()).getError());
	}

	@Test
	public void testGetAllMasterIsDeleted() throws IOException {
		ServiceItem service = service();
		service.setDeleted(true);
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", ServiceItem.class)).thenReturn(service);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.services.getAllItems(service, monitor()).getError());
	}

	@Test
	public void testGetAllMaster() throws IOException {
		ServiceItem service = service();
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");
		map.put("2", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", ServiceItem.class)).thenReturn(service);
		assertNull(this.services.getAllItems(service, monitor()).getError());
	}
}
