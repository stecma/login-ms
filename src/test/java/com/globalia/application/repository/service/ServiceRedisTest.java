package com.globalia.application.repository.service;

import com.globalia.HelperTest;
import com.globalia.dto.login.ServiceItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ServiceRedisTest extends HelperTest {

	@InjectMocks
	private ServiceRedis service;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(ServiceRedis.class, this.service, "servicesKey");
	}

	@Test
	public void testGetServiceNoJson() {
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.service.getItem(service(), monitor()).getError());
	}

	@Test
	public void testGetServiceIOException() throws IOException {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.service.getItem(service(), monitor()).getError());
	}

	@Test
	public void testGetServiceRunTimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.service.getItem(service(), monitor()).getError());
	}

	@Test
	public void testGetGroupJsonNull() throws IOException {
		when(this.client.get(nullable(String.class), anyString())).thenReturn(null);
		when(this.jsonHandler.fromJson(nullable(String.class), (Class<?>) any())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.service.getItem(service(), monitor()).getError());
	}

	@Test
	public void testGetServiceIsDelete() throws IOException {
		ServiceItem service = service();
		service.setDeleted(true);

		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(service);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.service.getItem(service, monitor()).getError());
	}

	@Test
	public void testGetService() throws IOException {
		ServiceItem service = service();

		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(service);
		assertNull(this.service.getItem(service, monitor()).getError());
	}
}
