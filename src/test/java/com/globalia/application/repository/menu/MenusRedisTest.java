package com.globalia.application.repository.menu;

import com.globalia.HelperTest;
import com.globalia.dto.login.MenuItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MenusRedisTest extends HelperTest {

	@InjectMocks
	private MenusRedis menus;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(MenusRedis.class, this.menus, "menusKey");
	}

	@Test
	public void testGetAllMenuRuntimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.menus.getAllItems(menu(), monitor()).getError());
	}

	@Test
	public void testGetAllMenuIOException() throws IOException {
		MenuItem menu = menu();

		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", MenuItem.class)).thenThrow(IOException.class);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.menus.getAllItems(menu, monitor()).getError());
	}

	@Test
	public void testGetAllMenuIsDeleted() throws IOException {
		MenuItem menu = menu();
		menu.setDeleted(true);

		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", MenuItem.class)).thenReturn(menu);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.menus.getAllItems(menu, monitor()).getError());
	}

	@Test
	public void testGetAllMenu() throws IOException {
		MenuItem menu = menu();

		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");
		map.put("2", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", MenuItem.class)).thenReturn(menu);
		assertNull(this.menus.getAllItems(menu, monitor()).getError());
	}
}
