package com.globalia.application.repository.menu;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.login.MenuItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveMenuRedisTest extends HelperTest {

	@InjectMocks
	private SaveMenuRedis save;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(SaveMenuRedis.class, this.save, "menusKey");
	}

	@Test
	public void testAddItemJsonProcessingException() throws JsonProcessingException {
		MenuItem menu = menu();
		menu.setId(null);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.addItem(menu, monitor()).getError());
	}

	@Test
	public void testAddItemRuntimeException() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		doThrow(RuntimeException.class).when(this.client).add(anyString(), anyString(), anyString());
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.redisAccess.getRediskey(anyString())).thenReturn("key");
		assertNotNull(this.save.addItem(menu(), monitor()).getError());
	}

	@Test
	public void testAddItem() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(menu(), monitor()).getError());
	}

	@Test
	public void testAddItemDeleted() throws JsonProcessingException {
		MenuItem menu = menu();
		menu.setDeleted(true);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(menu, monitor()).getError());
	}

	@Test
	public void testGetOrder() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.keys(anyString())).thenReturn(Set.of("1"));
		assertEquals(2, this.save.getOrder());
	}
}
