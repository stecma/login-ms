package com.globalia.application.repository.menu;

import com.globalia.HelperTest;
import com.globalia.dto.login.MenuItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MenuRedisTest extends HelperTest {

	@InjectMocks
	private MenuRedis menu;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(MenuRedis.class, this.menu, "menusKey");
	}

	@Test
	public void testGetMenuNoJson() {
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.menu.getItem(menu(), monitor()).getError());
	}

	@Test
	public void testGetMenuIOException() throws IOException {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.menu.getItem(menu(), monitor()).getError());
	}

	@Test
	public void testGetMenuRunTimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.menu.getItem(menu(), monitor()).getError());
	}

	@Test
	public void testGetMenuJsonNull() throws IOException {
		when(this.client.get(nullable(String.class), anyString())).thenReturn(null);
		when(this.jsonHandler.fromJson(nullable(String.class), (Class<?>) any())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.menu.getItem(menu(), monitor()).getError());
	}

	@Test
	public void testGetMenuIsDelete() throws IOException {
		MenuItem item = menu();
		item.setDeleted(true);

		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(item);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.menu.getItem(item, monitor()).getError());
	}

	@Test
	public void testGetMenu() throws IOException {
		MenuItem item = menu();
		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(item);
		assertNull(this.menu.getItem(item, monitor()).getError());
	}
}
