package com.globalia.application.repository.user;

import com.globalia.HelperTest;
import com.globalia.dto.login.UserItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class UserRedisTest extends HelperTest {

	@InjectMocks
	private UserRedis user;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(UserRedis.class, this.user, "usersKey");
	}

	@Test
	public void testGetUserNoJson() {
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.user.getItem(user(), monitor()).getError());
	}

	@Test
	public void testGetUserIOException() throws IOException {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.user.getItem(user(), monitor()).getError());
	}

	@Test
	public void testGetUserRunTimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.user.getItem(user(), monitor()).getError());
	}

	@Test
	public void testGetUserJsonNull() throws IOException {
		when(this.client.get(nullable(String.class), anyString())).thenReturn(null);
		when(this.jsonHandler.fromJson(nullable(String.class), (Class<?>) any())).thenReturn(null);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.user.getItem(user(), monitor()).getError());
	}

	@Test
	public void testGetUserIsDelete() throws IOException {
		UserItem item = user();
		item.setDeleted(true);

		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", UserItem.class)).thenReturn(item);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.user.getItem(item, monitor()).getError());
	}

	@Test
	public void testGetUser() throws IOException {
		UserItem item = user();

		when(this.client.get(nullable(String.class), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson("json", UserItem.class)).thenReturn(item);
		assertNull(this.user.getItem(item, monitor()).getError());
	}
}
