package com.globalia.application.repository.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.login.UserItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveUserRedisTest extends HelperTest {

	@InjectMocks
	private SaveUserRedis save;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(SaveUserRedis.class, this.save, "usersKey");
	}

	@Test
	public void testAddItemJsonProcessingException() throws JsonProcessingException {
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.addItem(user(), monitor()).getError());
	}

	@Test
	public void testAddItemRuntimeException() throws JsonProcessingException {
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		doThrow(RuntimeException.class).when(this.client).add(anyString(), anyString(), anyString());
		assertNotNull(this.save.addItem(user(), monitor()).getError());
	}

	@Test
	public void testAddItem() throws JsonProcessingException {
		UserItem user = user();
		user.setCreated(true);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(user, monitor()).getError());
	}

	@Test
	public void testAddItemDeleted() throws JsonProcessingException {
		UserItem user = user();
		user.setDeleted(true);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(user, monitor()).getError());
	}
}
