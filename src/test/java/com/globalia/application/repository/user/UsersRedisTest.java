package com.globalia.application.repository.user;

import com.globalia.HelperTest;
import com.globalia.dto.login.UserItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class UsersRedisTest extends HelperTest {

	@InjectMocks
	private UsersRedis users;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(UsersRedis.class, this.users, "usersKey");
	}

	@Test
	public void testGetAllUserRuntimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.users.getAllItems(user(), monitor()).getError());
	}

	@Test
	public void testGetAllUserIOException() throws IOException {
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", UserItem.class)).thenThrow(IOException.class);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.users.getAllItems(user(), monitor()).getError());
	}

	@Test
	public void testGetAllUserIsDeleted() throws IOException {
		UserItem user = user();
		user.setDeleted(true);

		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(user);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.users.getAllItems(user, monitor()).getError());
	}

	@Test
	public void testGetAllUser() throws IOException {
		UserItem user = user();
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");
		map.put("2", "json");

		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(user);
		assertNull(this.users.getAllItems(user, monitor()).getError());
	}
}
