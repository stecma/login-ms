package com.globalia.application.repository.master;

import com.globalia.HelperTest;
import com.globalia.dto.login.MasterItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MasterRedisTest extends HelperTest {

	@InjectMocks
	private MasterRedis master;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(MasterRedis.class, this.master, "mastersKey");
	}

	@Test
	public void testGetMasterNoJson() {
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetMasterIOException() throws IOException {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetMasterRunTimeException() {
		when(this.redisAccess.getRediskey(anyString())).thenReturn("dev");
		when(this.client.get(anyString(), anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetMasterJsonNull() throws IOException {
		when(this.client.get(anyString(), anyString())).thenReturn(null);
		when(this.jsonHandler.fromJson(nullable(String.class), (Class<?>) any())).thenReturn(null);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetMasterIsDelete() throws IOException {
		MasterItem master = master();
		master.setDeleted(true);

		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(master);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.master.getItem(master, monitor()).getError());
	}

	@Test
	public void testGetMaster() throws IOException {
		MasterItem master = master();

		when(this.client.get(anyString(), anyString())).thenReturn("json");
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(master);
		assertNull(this.master.getItem(master, monitor()).getError());
	}
}
