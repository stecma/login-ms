package com.globalia.application.repository.master;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.HelperTest;
import com.globalia.dto.login.MasterItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveMasterRedisTest extends HelperTest {

	@InjectMocks
	private SaveMasterRedis save;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(SaveMasterRedis.class, this.save, "mastersKey");
	}

	@Test
	public void testAddItemJsonProcessingException() throws JsonProcessingException {
		MasterItem master = master();
		master.setId(null);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNotNull(this.save.addItem(master, monitor()).getError());
	}

	@Test
	public void testAddItemRuntimeException() throws JsonProcessingException {
		MasterItem master = master();
		master.setDeleted(true);
		when(this.jsonHandler.toJson(any())).thenReturn("json");
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		doThrow(RuntimeException.class).when(this.client).add(anyString(), anyString(), anyString());
		assertNotNull(this.save.addItem(master, monitor()).getError());
	}

	@Test
	public void testAddItem() throws JsonProcessingException {
		MasterItem master = master();
		master.setCreated(true);

		when(this.jsonHandler.toJson(any())).thenReturn("json");
		assertNull(this.save.addItem(master, monitor()).getError());
	}
}
