package com.globalia.application.repository.master;

import com.globalia.HelperTest;
import com.globalia.dto.login.MasterItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class MastersRedisTest extends HelperTest {

	@InjectMocks
	private MastersRedis masters;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		redisKey(MastersRedis.class, this.masters, "mastersKey");
	}

	@Test
	public void testGetAllMasterRuntimeException() {
		MasterItem master = master();

		when(this.client.patternKeys(anyString())).thenReturn(Set.of(String.format("%s:%s", "key", master.getEntity())));
		when(this.client.getAll(anyString())).thenThrow(RuntimeException.class);
		when(this.errorComponent.addError(any(), anyString(), any(), anyString(), anyString(), anyInt())).thenReturn(new Error());
		assertNotNull(this.masters.getAllItems(master, monitor()).getError());
	}

	@Test
	public void testGetAllMasterIOException() throws IOException {
		MasterItem master = master();
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.client.patternKeys(anyString())).thenReturn(Set.of(String.format("%s:%s", "key", master.getEntity())));
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenThrow(IOException.class);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.masters.getAllItems(master, monitor()).getError());
	}

	@Test
	public void testGetAllMasterIsDeleted() throws IOException {
		MasterItem master = master();
		master.setDeleted(true);
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.client.patternKeys(anyString())).thenReturn(Set.of(String.format("%s:%s", "key", master.getEntity())));
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(master);
		when(this.errorComponent.addError(anyString(), any(), any())).thenReturn(new Error());
		assertNotNull(this.masters.getAllItems(master, monitor()).getError());
	}

	@Test
	public void testGetAllMaster() throws IOException {
		MasterItem master = master();
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");
		map.put("2", "json");

		when(this.client.patternKeys(anyString())).thenReturn(Set.of(String.format("%s:%s", "key", master.getEntity())));
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(master);
		assertNull(this.masters.getAllItems(master, monitor()).getError());
	}

	@Test
	public void testGetAllMasterNoEntity() throws IOException {
		MasterItem master = master();
		master.setEntity(null);
		Map<String, Object> map = new HashMap<>();
		map.put("1", "json");

		when(this.client.patternKeys(anyString())).thenReturn(Set.of(String.format("%s:*", "key")));
		when(this.client.getAll(anyString())).thenReturn(map);
		when(this.jsonHandler.fromJson("json", MasterItem.class)).thenReturn(master);
		assertNull(this.masters.getAllItems(master, monitor()).getError());
	}

	@Test
	public void testFindCodesRuntimeException() {
		when(this.client.keys(anyString())).thenThrow(RuntimeException.class);
		assertNull(this.masters.findCodes(master().getEntity()));
	}

	@Test
	public void testFindCodes() {
		when(this.client.keys(anyString())).thenReturn(Set.of("CIA"));
		assertNotNull(this.masters.findCodes(master().getEntity()));
	}
}
