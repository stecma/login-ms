package com.globalia.application.service.group;

import com.globalia.HelperTest;
import com.globalia.application.repository.group.GroupRedis;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupTest extends HelperTest {

	@InjectMocks
	private Group group;
	@Mock
	private GroupRedis groupRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRedisError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());

		when(this.groupRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.group.getItem(group(), monitor()).getError());
	}

	@Test
	public void testGetItemNoCodes() {
		GroupItem group = group();
		group.setCodes(null);

		ItemResponse item = new ItemResponse();
		item.setGroup(group);

		when(this.groupRedis.getItem(any(), any())).thenReturn(item);
		assertNull(this.group.getItem(group, monitor()).getError());
	}

	@Test
	public void testGetItemEmptyCodes() {
		GroupItem group = group();
		group.setCodes(new HashSet<>());

		ItemResponse item = new ItemResponse();
		item.setGroup(group);

		when(this.groupRedis.getItem(any(), any())).thenReturn(item);
		assertNull(this.group.getItem(group, monitor()).getError());
	}

	@Test
	public void testGetItem() {
		GroupItem group = group();

		ItemResponse item = new ItemResponse();
		item.setGroup(group);

		when(this.groupRedis.getItem(any(), any())).thenReturn(item);
		when(this.services.getAllServices(any())).thenReturn(new HashMap<>());
		when(this.services.addServices(any(), any())).thenReturn(null);
		assertNull(this.group.getItem(group, monitor()).getError());
	}
}
