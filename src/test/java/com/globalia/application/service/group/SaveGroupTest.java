package com.globalia.application.service.group;

import com.globalia.HelperTest;
import com.globalia.application.repository.group.SaveGroupRedis;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveGroupTest extends HelperTest {

	@InjectMocks
	private SaveGroup saveGroup;
	@Mock
	private SaveGroupRedis groupRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateItem() {
		assertNull(this.saveGroup.createItem(group(), monitor()));
	}

	@Test
	public void testUpdateItem() {
		assertNull(this.saveGroup.updateItem(group(), monitor()));
	}

	@Test
	public void testDeleteItem() {
		assertNull(this.saveGroup.deleteItem(group(), monitor()));
	}
}
