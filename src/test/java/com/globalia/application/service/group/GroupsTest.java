package com.globalia.application.service.group;

import com.globalia.HelperTest;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.GroupItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class GroupsTest extends HelperTest {

	@InjectMocks
	private Groups groups;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItem() {
		assertNull(this.groups.getAllItems(group(), monitor()));
	}

	@Test
	public void testAddGroupsNullCodes() {
		assertNull(this.groups.addGroups(null, null, null));
	}

	@Test
	public void testAddGroupsDiffCodes() {
		PairValue pairGroup = new PairValue();
		pairGroup.setKey("services");
		pairGroup.setValue(new LinkedHashSet<>(Set.of("1")));

		assertNull(this.groups.addGroups(Set.of(pairGroup), null, null));
	}

	@Test
	public void testAddGroupsEmptyMap() {
		PairValue pairGroup = new PairValue();
		pairGroup.setKey("groups");
		pairGroup.setValue(new LinkedHashSet<>(Set.of("1")));

		assertNull(this.groups.addGroups(Set.of(pairGroup), new HashMap<>(), null));
	}

	@Test
	public void testAddGroups() {
		PairValue pairGroup = new PairValue();
		pairGroup.setKey("groups");
		pairGroup.setValue(new LinkedHashSet<>(Set.of("1")));

		Map<String, GroupItem> map = new HashMap<>();
		map.put("1", group());

		when(this.services.addServices(any(), any())).thenReturn(null);
		assertNotNull(this.groups.addGroups(Set.of(pairGroup), map, null));
	}

	@Test
	public void testGetAllGroupsRedisError() {
		AllItemsResponse groups = new AllItemsResponse();
		groups.setError(new Error());

		when(this.groupRedis.getAllItems(any(), any())).thenReturn(groups);
		assertEquals(0, this.groups.getAllGroups(monitor()).size());
	}

	@Test
	public void testGetAllGroups() {
		GroupItem group = group();

		AllItemsResponse groups = new AllItemsResponse();
		groups.setGroups(new LinkedHashSet<>(Set.of(group)));

		when(this.groupRedis.getAllItems(any(), any())).thenReturn(groups);
		assertEquals(1, this.groups.getAllGroups(monitor()).size());
	}

	@Test
	public void testCreateNewGroup() {
		assertNotNull(this.groups.createNewGroup(group()));
	}
}
