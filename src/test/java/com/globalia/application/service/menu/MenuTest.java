package com.globalia.application.service.menu;

import com.globalia.HelperTest;
import com.globalia.application.repository.menu.MenuRedis;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.menu.Menu;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MenuItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MenuTest extends HelperTest {

	@InjectMocks
	private Menu menu;
	@Mock
	private MenuRedis menuRedis;
	@Mock
	private Groups groups;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRedisError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());

		when(this.menuRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.menu.getItem(menu(), monitor()));
	}

	@Test
	public void testGetItemNoCodes() {
		MenuItem menu = menu();
		menu.setCodes(null);

		ItemResponse item = new ItemResponse();
		item.setMenu(menu);

		when(this.menuRedis.getItem(any(), any())).thenReturn(item);
		assertNull(this.menu.getItem(menu, monitor()).getError());
	}

	@Test
	public void testGetItemEmptyCodes() {
		MenuItem menu = menu();
		menu.setCodes(new HashSet<>());

		ItemResponse item = new ItemResponse();
		item.setMenu(menu);

		when(this.menuRedis.getItem(any(), any())).thenReturn(item);
		assertNull(this.menu.getItem(menu, monitor()).getError());
	}

	@Test
	public void testGetItem() {
		MenuItem menu = menu();

		ItemResponse item = new ItemResponse();
		item.setMenu(menu);

		when(this.menuRedis.getItem(any(), any())).thenReturn(item);
		when(this.services.getAllServices(any())).thenReturn(new HashMap<>());
		when(this.services.addServices(any(), any())).thenReturn(null);
		when(this.groups.getAllGroups(any())).thenReturn(new HashMap<>());
		when(this.groups.addGroups(any(), any(), any())).thenReturn(null);
		assertNull(this.menu.getItem(menu, monitor()).getError());
	}
}
