package com.globalia.application.service.menu;

import com.globalia.HelperTest;
import com.globalia.application.repository.menu.MenusRedis;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.menu.Menus;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MenusTest extends HelperTest {

	@InjectMocks
	private Menus menus;
	@Mock
	private MenusRedis menusRedis;
	@Mock
	private Groups groups;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemRedisError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error());

		when(this.menusRedis.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.menus.getAllItems(menu(), monitor()));
	}

	@Test
	public void testGetAllItemNoCodes() {
		MenuItem menu = menu();
		menu.setCodes(null);

		AllItemsResponse response = new AllItemsResponse();
		response.setMenus(new LinkedHashSet<>(Set.of(menu)));

		Map<String, ServiceItem> srvMap = new HashMap<>();
		srvMap.put("1", new ServiceItem());
		Map<String, GroupItem> grpMap = new HashMap<>();
		grpMap.put("1", new GroupItem());

		when(this.services.getAllServices(any())).thenReturn(srvMap);
		when(this.groups.getAllGroups(any())).thenReturn(grpMap);
		when(this.menusRedis.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.menus.getAllItems(menu, monitor()));
	}

	@Test
	public void testGetAllItemEmptyCodes() {
		MenuItem menu = menu();
		menu.setCodes(new HashSet<>());

		AllItemsResponse response = new AllItemsResponse();
		response.setMenus(new LinkedHashSet<>(Set.of(menu)));

		Map<String, ServiceItem> srvMap = new HashMap<>();
		srvMap.put("1", new ServiceItem());
		Map<String, GroupItem> grpMap = new HashMap<>();
		grpMap.put("1", new GroupItem());

		when(this.services.getAllServices(any())).thenReturn(srvMap);
		when(this.groups.getAllGroups(any())).thenReturn(grpMap);
		when(this.menusRedis.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.menus.getAllItems(menu, monitor()));
	}

	@Test
	public void testGetAllItems() {
		MenuItem menu = menu();

		AllItemsResponse response = new AllItemsResponse();
		response.setMenus(new LinkedHashSet<>(Set.of(menu)));

		Map<String, ServiceItem> srvMap = new HashMap<>();
		srvMap.put("1", new ServiceItem());
		Map<String, GroupItem> grpMap = new HashMap<>();
		grpMap.put("1", new GroupItem());

		when(this.services.getAllServices(any())).thenReturn(srvMap);
		when(this.services.addServices(any(), anyMap())).thenReturn(null);
		when(this.groups.getAllGroups(any())).thenReturn(grpMap);
		when(this.groups.addGroups(any(), anyMap(), anyMap())).thenReturn(null);
		when(this.menusRedis.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.menus.getAllItems(menu, monitor()));
	}

	@Test
	public void testGetAllGroupsRedisError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error());

		when(this.menusRedis.getAllItems(any(), any())).thenReturn(response);
		assertEquals(0, this.menus.getAllMenus(monitor()).size());
	}

	@Test
	public void testGetAllGroups() {
		AllItemsResponse response = new AllItemsResponse();
		response.setMenus(new LinkedHashSet<>(Set.of(menu())));

		when(this.menusRedis.getAllItems(any(), any())).thenReturn(response);
		assertEquals(1, this.menus.getAllMenus(monitor()).size());
	}

	@Test
	public void testCreateNewMenu() {
		assertNotNull(this.menus.createNewMenu(menu()));
	}
}
