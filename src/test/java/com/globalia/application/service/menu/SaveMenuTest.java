package com.globalia.application.service.menu;

import com.globalia.HelperTest;
import com.globalia.application.repository.menu.SaveMenuRedis;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveMenuTest extends HelperTest {

	@InjectMocks
	private SaveMenu save;
	@Mock
	private SaveMenuRedis menuRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateItem() {
		when(this.menuRedis.getOrder()).thenReturn(1);
		assertNull(this.save.createItem(menu(), monitor()));
	}

	@Test
	public void testUpdateItem() {
		assertNull(this.save.updateItem(menu(), monitor()));
	}

	@Test
	public void testDeleteItem() {
		assertNull(this.save.deleteItem(menu(), monitor()));
	}
}
