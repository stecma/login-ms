package com.globalia.application.service.menu;

import com.globalia.dto.login.MenuItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MenuComparatorTest {

	@InjectMocks
	private MenuComparator menuComparator;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCompare() {
		MenuItem menu1 = new MenuItem();
		menu1.setOrder(1);

		MenuItem menu2 = new MenuItem();
		menu2.setOrder(2);

		assertEquals(-1, this.menuComparator.compare(menu1, menu2));
	}

	@Test
	public void testCompare2() {
		MenuItem menu1 = new MenuItem();
		menu1.setOrder(1);

		MenuItem menu2 = new MenuItem();
		menu2.setOrder(2);

		int result = this.menuComparator.compare(menu2, menu1);
		assertEquals(1, this.menuComparator.compare(menu2, menu1));
	}
}
