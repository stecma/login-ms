package com.globalia.application.service.master;

import com.globalia.HelperTest;
import com.globalia.application.repository.master.MasterRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MasterTest extends HelperTest {

	@InjectMocks
	private Master master;
	@Mock
	private MasterRedis masterRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemMasterError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error());

		when(this.masterRedis.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.master.getItem(master(), monitor()).getError());
	}

	@Test
	public void testGetItemNoCodes() {
		MasterItem master = master();
		master.setCodes(null);

		ItemResponse response = new ItemResponse();
		response.setMaster(master);

		when(this.masterRedis.getItem(any(), any())).thenReturn(response);
		assertNull(this.master.getItem(master, monitor()).getError());
	}

	@Test
	public void testGetItem() {
		MasterItem master = master();

		ItemResponse response = new ItemResponse();
		response.setMaster(master);

		when(this.masterRedis.getItem(any(), any())).thenReturn(response);
		assertNull(this.master.getItem(master, monitor()).getError());
	}
}
