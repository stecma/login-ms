package com.globalia.application.service.master;

import com.globalia.HelperTest;
import com.globalia.application.repository.master.MastersRedis;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class MastersTest extends HelperTest {

	@InjectMocks
	private Masters masters;
	@Mock
	private MastersRedis mastersRedis;
	@Mock
	private Master master;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemRedisError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error());

		when(this.mastersRedis.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.masters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testGetAllItem() {
		AllItemsResponse response = new AllItemsResponse();
		response.setMasters(new LinkedHashSet<>(Set.of(master())));

		when(this.mastersRedis.getAllItems(any(), any())).thenReturn(response);
		assertNull(this.masters.getAllItems(master(), monitor()).getError());
	}

	@Test
	public void testValidateKeyTrue() {
		MasterItem master = master();
		when(this.mastersRedis.findCodes(anyString())).thenReturn(Set.of("1"));
		assertTrue(this.masters.validateKey(master.getEntity(), master.getId()));
	}

	@Test
	public void testValidateKeyFalse() {
		MasterItem master = master();
		when(this.mastersRedis.findCodes(anyString())).thenReturn(Set.of("2"));
		assertFalse(this.masters.validateKey(master.getEntity(), master.getId()));
	}
}
