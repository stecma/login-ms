package com.globalia.application.service.master;

import com.globalia.HelperTest;
import com.globalia.application.repository.master.SaveMasterRedis;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveMasterTest extends HelperTest {

	@InjectMocks
	private SaveMaster save;
	@Mock
	private SaveMasterRedis masterRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateItem() {
		assertNull(this.save.createItem(master(), monitor()));
	}

	@Test
	public void testUpdateItem() {
		assertNull(this.save.updateItem(master(), monitor()));
	}

	@Test
	public void testDeleteItem() {
		assertNull(this.save.deleteItem(master(), monitor()));
	}
}
