package com.globalia.application.service.user;

import com.globalia.HelperTest;
import com.globalia.application.repository.user.SaveUserRedis;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.UserItem;
import com.globalia.enumeration.LogType;
import com.globalia.security.AESPassEncrypter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveUserTest extends HelperTest {

	@InjectMocks
	private SaveUser save;
	@Mock
	private SaveUserRedis userRedis;
	@Mock
	private AESPassEncrypter encrypter;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateItemEncryptError() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		when(this.encrypter.encrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenThrow(NoSuchAlgorithmException.class);
		assertNotNull(this.save.createItem(user(), monitor()).getError());
	}

	@Test
	public void testCreateItemNoPass() {
		UserItem user = user();
		user.setPass(null);

		when(this.userRedis.addItem(any(), any())).thenReturn(new ItemResponse());
		assertNull(this.save.createItem(user, monitor()).getError());
	}

	@Test
	public void testCreateItem() {
		when(this.userRedis.addItem(any(), any())).thenReturn(new ItemResponse());
		assertNull(this.save.createItem(user(), monitor()).getError());
	}

	@Test
	public void testUpdateItemError() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		when(this.encrypter.encrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenThrow(NoSuchAlgorithmException.class);
		assertNotNull(this.save.updateItem(user(), true, monitor()).getError());
	}

	@Test
	public void testUpdateItem() {
		assertNull(this.save.updateItem(user(), true, monitor()));
	}

	@Test
	public void testUpdateItemNoEncrypt() {
		assertNull(this.save.updateItem(user(), monitor()));
	}

	@Test
	public void testDeleteItem() {
		assertNull(this.save.deleteItem(user(), monitor()));
	}
}
