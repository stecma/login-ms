package com.globalia.application.service.user;

import com.globalia.HelperTest;
import com.globalia.application.repository.user.UsersRedis;
import com.globalia.dto.login.AllItemsResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UsersTest extends HelperTest {

	@InjectMocks
	private Users users;
	@Mock
	private UsersRedis usersRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItems() {
		when(this.usersRedis.getAllItems(any(), any())).thenReturn(new AllItemsResponse());
		assertNotNull(this.users.getAllItems(user(), monitor()));
	}
}
