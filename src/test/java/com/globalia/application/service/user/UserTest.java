package com.globalia.application.service.user;

import com.globalia.Context;
import com.globalia.HelperTest;
import com.globalia.application.repository.user.UserRedis;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.master.Masters;
import com.globalia.application.service.menu.Menus;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.ItemResponse;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserTest extends HelperTest {

	@InjectMocks
	private User user;
	@Mock
	private UserRedis userRedis;
	@Mock
	private Masters masters;
	@Mock
	private Master master;
	@Mock
	private Groups groups;
	@Mock
	private Menus menus;
	@Mock
	private Context context;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemRedisError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());

		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetItemNoCodes() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setCodes(null);
		item.getUser().setConfiguration(null);

		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetItemEmptyCodes() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setCodes(new LinkedHashSet<>());
		item.getUser().setConfiguration(new LinkedHashSet<>());

		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetItemCompleteBusinessError() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setConfiguration(new LinkedHashSet<>());

		AllItemsResponse masters = new AllItemsResponse();
		masters.setError(new Error());

		when(this.master.getItem(any(), any())).thenReturn(new ItemResponse());
		when(this.masters.getAllItems(any(), any())).thenReturn(masters);
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetItemCompleteBusiness() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setConfiguration(new LinkedHashSet<>());

		when(this.master.getItem(any(), any())).thenReturn(new ItemResponse());
		when(this.masters.getAllItems(any(), any())).thenReturn(this.getBusiness());
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetItemCompleteMenuNoChild() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());

		when(this.master.getItem(any(), any())).thenReturn(new ItemResponse());
		when(this.masters.getAllItems(any(), any())).thenReturn(this.getBusiness());
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetItemCompleteMenuNoConfigCodes() {
		ItemResponse item = new ItemResponse();
		item.setUser(user(true));

		Map<String, ServiceItem> srvMap = new HashMap<>();
		srvMap.put("1", service());
		Map<String, GroupItem> grpMap = new HashMap<>();
		grpMap.put("1", group());
		Map<String, MenuItem> mnuMap = new HashMap<>();
		mnuMap.put("1", menu());

		when(this.menus.getAllMenus(any())).thenReturn(mnuMap);
		when(this.groups.getAllGroups(any())).thenReturn(grpMap);
		when(this.services.getAllServices(any())).thenReturn(srvMap);
		when(this.master.getItem(any(), any())).thenReturn(new ItemResponse());
		when(this.masters.getAllItems(any(), any())).thenReturn(this.getBusiness());
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetItemCompleteMenu() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());

		Map<String, ServiceItem> srvMap = new HashMap<>();
		srvMap.put("1", service());
		Map<String, GroupItem> grpMap = new HashMap<>();
		grpMap.put("1", group());
		Map<String, MenuItem> mnuMap = new HashMap<>();
		mnuMap.put("1", menu());

		when(this.menus.getAllMenus(any())).thenReturn(mnuMap);
		when(this.menus.createNewMenu(any())).thenReturn(menu());
		when(this.groups.getAllGroups(any())).thenReturn(grpMap);
		when(this.groups.createNewGroup(any())).thenReturn(group());
		when(this.services.getAllServices(any())).thenReturn(srvMap);
		when(this.master.getItem(any(), any())).thenReturn(new ItemResponse());
		when(this.masters.getAllItems(any(), any())).thenReturn(this.getBusiness());
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getItem(user(), monitor()));
	}

	@Test
	public void testGetUserError() {
		ItemResponse item = new ItemResponse();
		item.setError(new Error());

		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getUser(user().getId(), monitor()).getError());
	}

	@Test
	public void testGetUserInactive() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setCodes(null);
		item.getUser().setConfiguration(null);
		item.getUser().setActive(false);

		when(this.masters.getAllItems(any(), any())).thenReturn(this.getBusiness());
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getUser(user().getId(), monitor()).getError());
	}

	@Test
	public void testGetUserInvalidEnv() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setCodes(null);
		item.getUser().setConfiguration(null);

		when(this.context.getEnvironment()).thenReturn("EMPTY");
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNotNull(this.user.getUser(user().getId(), monitor()).getError());
	}

	@Test
	public void testGetUser() {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setCodes(null);
		item.getUser().setConfiguration(null);

		when(this.context.getEnvironment()).thenReturn("DEV");
		when(this.userRedis.getItem(any(), any())).thenReturn(item);
		assertNull(this.user.getUser(user().getId(), monitor()).getError());
	}

	private AllItemsResponse getBusiness() {
		MasterItem child1 = new MasterItem();
		child1.setId("1");
		child1.setEntity(MasterType.COMPANY.name());

		MasterItem child2 = new MasterItem();
		child2.setId("2");
		child2.setEntity(MasterType.COMPANY.name());

		MasterItem master = new MasterItem();
		master.setId("1");
		master.setEntity(MasterType.BUSINESS_APP.name());
		master.setMasters(Set.of(child1, child2));

		AllItemsResponse masters = new AllItemsResponse();
		masters.setMasters(new LinkedHashSet<>(Set.of(master)));
		return masters;
	}
}
