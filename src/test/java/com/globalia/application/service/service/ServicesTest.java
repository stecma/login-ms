package com.globalia.application.service.service;

import com.globalia.HelperTest;
import com.globalia.application.repository.service.ServicesRedis;
import com.globalia.application.service.service.Services;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.dto.login.ServiceItem;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ServicesTest extends HelperTest {

	@InjectMocks
	private Services services;
	@Mock
	private ServicesRedis servicesRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItem() {
		assertNull(this.services.getAllItems(service(), monitor()));
	}

	@Test
	public void testAddServicesNoCodes() {
		assertNull(this.services.addServices(null, null));
	}

	@Test
	public void testAddServicesDiffCodes() {
		PairValue pairGroup = new PairValue();
		pairGroup.setKey("groups");
		pairGroup.setValue(new LinkedHashSet<>(Set.of("1")));

		assertNull(this.services.addServices(Set.of(pairGroup), null));
	}

	@Test
	public void testAddServicesEmptyMap() {
		PairValue pairGroup = new PairValue();
		pairGroup.setKey("services");
		pairGroup.setValue(new LinkedHashSet<>(Set.of("1")));

		assertNull(this.services.addServices(Set.of(pairGroup), new HashMap<>()));
	}

	@Test
	public void testAddGroups() {
		PairValue pairGroup = new PairValue();
		pairGroup.setKey("services");
		pairGroup.setValue(new LinkedHashSet<>(Set.of("1")));

		Map<String, ServiceItem> map = new HashMap<>();
		map.put("1", service());

		assertNotNull(this.services.addServices(Set.of(pairGroup), map));
	}

	@Test
	public void testGetAllServicesRedisError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error());

		when(this.servicesRedis.getAllItems(any(), any())).thenReturn(response);
		assertEquals(0, this.services.getAllServices(monitor()).size());
	}

	@Test
	public void testGetAllServices() {
		ServiceItem service = service();

		AllItemsResponse response = new AllItemsResponse();
		response.setServices(new LinkedHashSet<>(Set.of(service)));

		when(this.servicesRedis.getAllItems(any(), any())).thenReturn(response);
		assertEquals(1, this.services.getAllServices(monitor()).size());
	}
}
