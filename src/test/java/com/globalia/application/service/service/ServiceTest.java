package com.globalia.application.service.service;

import com.globalia.HelperTest;
import com.globalia.application.repository.service.ServiceRedis;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ServiceTest extends HelperTest {

	@InjectMocks
	private Service service;
	@Mock
	private ServiceRedis serviceRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItem() {
		assertNull(this.service.getItem(service(), monitor()));
	}
}
