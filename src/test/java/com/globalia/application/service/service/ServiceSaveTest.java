package com.globalia.application.service.service;

import com.globalia.HelperTest;
import com.globalia.application.repository.service.SaveServiceRedis;
import com.globalia.application.service.service.ServiceSave;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
public class ServiceSaveTest extends HelperTest {

	@InjectMocks
	private ServiceSave save;
	@Mock
	private SaveServiceRedis serviceRedis;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testCreateItem() {
		assertNull(this.save.createItem(service(), monitor()));
	}

	@Test
	public void testUpdateItem() {
		assertNull(this.save.updateItem(service(), monitor()));
	}

	@Test
	public void testDeleteItem() {
		assertNull(this.save.deleteItem(service(), monitor()));
	}
}
