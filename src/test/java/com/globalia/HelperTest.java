package com.globalia;

import com.globalia.api.login.ApiRQ;
import com.globalia.application.producer.MessageProducer;
import com.globalia.application.repository.group.GroupsRedis;
import com.globalia.application.service.service.Services;
import com.globalia.dto.I18n;
import com.globalia.dto.KafkaItem;
import com.globalia.dto.PairValue;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.LoginItem;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.Url;
import com.globalia.dto.login.UserItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.dto.login.UserMenuCodes;
import com.globalia.enumeration.login.MasterType;
import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import com.globalia.redis.RedisAccess;
import com.globalia.redis.RedisClient;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.mockito.Mock;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class HelperTest {

	@Mock
	protected ProceedingJoinPoint joinPoint;
	@Mock
	protected MethodSignature signature;
	@Mock
	protected JsonHandler jsonHandler;
	@Mock
	protected RedisAccess redisAccess;
	@Mock
	protected Errors errorComponent;
	@Mock
	protected RedisClient client;
	@Mock
	protected GroupsRedis groupRedis;
	@Mock
	protected Services services;
	@Mock
	protected MonitorHandler handler;
	@Mock
	protected MessageProducer producer;
	@Mock
	protected ReplyingKafkaTemplate<String, String, String> replyKafkaTemplate;

	public static Monitor monitor() {
		Monitor monitor = new Monitor();
		monitor.setLogs(new HashSet<>());
		monitor.setStats(new HashSet<>());
		return monitor;
	}

	public static ApiRQ login() {
		ApiRQ request = new ApiRQ();
		request.setLogin(new LoginItem());
		request.getLogin().setUser("User");
		request.getLogin().setPass("Pass");
		return request;
	}

	public static ApiRQ userRQ() {
		ApiRQ request = new ApiRQ();
		request.setUser(user());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ serviceRQ() {
		ApiRQ request = new ApiRQ();
		request.setService(service());
		request.getService().setUrl(new Url());
		request.getService().getUrl().setRoute("test");
		request.getService().setEnvironment("TEST");
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ menuRQ() {
		ApiRQ request = new ApiRQ();
		request.setMenu(menu());
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ groupRQ() {
		ApiRQ request = new ApiRQ();
		request.setGroup(group());
		request.getGroup().setEnvironment("test");
		request.setMonitor(monitor());
		return request;
	}

	public static ApiRQ masterRQ() {
		ApiRQ request = new ApiRQ();
		request.setMaster(master());
		request.setMonitor(monitor());
		return request;
	}

	public static void jwtSecret(final Class<?> classes, final Object obj) {
		try {
			Field field = classes.getSuperclass().getDeclaredField("jwtSecret");
			field.setAccessible(true);
			field.set(obj, "JwtSecret0123456");
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static UserItem user() {
		return user(false);
	}

	public static UserItem user(final boolean noCodes) {
		Url uri = new Url();
		uri.setRoute("/test");
		uri.setReadOnly(true);

		ServiceItem service = new ServiceItem();
		service.setId("1");
		service.setUrl(uri);

		ServiceItem service2 = new ServiceItem();
		service2.setId("2");
		service2.setUrl(uri);

		GroupItem group = new GroupItem();
		group.setId("1");
		group.setServices(new LinkedHashSet<>() {{
			add(service);
		}});

		MenuItem menu2 = new MenuItem();
		menu2.setId("1");
		menu2.setEnvironment("DEV");
		menu2.setCompany("CIA");
		menu2.setGroups(new LinkedHashSet<>() {{
			add(group);
		}});
		menu2.setServices(new LinkedHashSet<>() {{
			add(service);
			add(service2);
		}});

		MasterItem cia = new MasterItem();
		cia.setId("CIA");
		MasterItem env = new MasterItem();
		env.setId("DEV");

		UserMenuCodes code1 = new UserMenuCodes();
		code1.setMenu("1");
		code1.setGroup("1");
		code1.setService("1");

		UserMenuCodes code2 = new UserMenuCodes();
		code2.setMenu("1");
		code2.setService("1");

		UserMenuCodes code3 = new UserMenuCodes();
		code3.setMenu("1");
		code3.setService("2");

		UserMenu menu = new UserMenu();
		menu.setEnvironment("DEV");
		menu.setCompany("CIA");
		menu.setMenus(new LinkedHashSet<>());
		menu.getMenus().add(menu2);
		if (!noCodes) {
			menu.setCodes(new LinkedHashSet<>() {{
				add(code1);
				add(code2);
				add(code3);
			}});
		}

		PairValue pair1 = new PairValue();
		pair1.setKey(MasterType.COMPANY.name());
		pair1.setValue(new LinkedHashSet<>(Set.of("1")));

		PairValue pair2 = new PairValue();
		pair2.setKey(MasterType.ENVIRONMENT.name());
		pair2.setValue(new LinkedHashSet<>(Set.of("1")));

		UserItem user = new UserItem();
		user.setId("1");
		user.setPass("passuser1");
		user.setName("test");
		user.setSurname("surname user1");
		user.setMail("mail@user1.com");
		user.setLanguage("ES");
		user.setEnvironments(new LinkedHashSet<>() {{
			add(env);
		}});
		user.setCompanies(new LinkedHashSet<>() {{
			add(cia);
		}});
		user.setConfiguration(new LinkedHashSet<>());
		user.getConfiguration().add(menu);
		user.setActive(true);
		user.setCodes(new LinkedHashSet<>(Set.of(pair1, pair2)));
		return user;
	}

	public static ServiceItem service() {
		Url url = new Url();
		url.setRoute("test");

		I18n i18n = new I18n();
		i18n.setKey("ES");
		i18n.setValue("test");

		ServiceItem service = new ServiceItem();
		service.setId("1");
		service.setEnvironment("test");
		service.setUrl(url);
		service.setNames(new LinkedHashSet<>());
		service.getNames().add(i18n);
		service.setDescriptions(new LinkedHashSet<>());
		service.getDescriptions().add(i18n);
		service.setUsers(Set.of("user"));
		service.setIcon("icon");

		return service;
	}

	public static GroupItem group() {
		I18n i18n = new I18n();
		i18n.setKey("ES");
		i18n.setValue("test");

		ServiceItem service = new ServiceItem();
		service.setId("1");

		PairValue pair = new PairValue();
		pair.setKey("services");
		pair.setValue(new LinkedHashSet<>(Set.of("1")));

		GroupItem group = new GroupItem();
		group.setId("1");
		group.setEnvironment("test");
		group.setUsers(Set.of("user"));
		group.setNames(new LinkedHashSet<>());
		group.getNames().add(i18n);
		group.setDescriptions(new LinkedHashSet<>());
		group.getDescriptions().add(i18n);
		group.setServices(new LinkedHashSet<>(Set.of(service)));
		group.setCodes(new LinkedHashSet<>(Set.of(pair)));

		return group;
	}

	public static MenuItem menu() {
		PairValue pairService = new PairValue();
		pairService.setKey("services");
		pairService.setValue(new LinkedHashSet<>(Set.of("1")));

		PairValue pairGroup = new PairValue();
		pairGroup.setKey("groups");
		pairGroup.setValue(new LinkedHashSet<>(Set.of("1")));

		ServiceItem srv = new ServiceItem();
		srv.setId("1");
		srv.setEnvironment("DEV");

		GroupItem grp = new GroupItem();
		grp.setId("1");
		grp.setEnvironment("DEV");
		grp.setServices(new LinkedHashSet<>());
		grp.getServices().add(srv);

		I18n i18n = new I18n();
		i18n.setKey("ES");
		i18n.setValue("test");

		MenuItem menu = new MenuItem();
		menu.setId("1");
		menu.setEnvironment("DEV");
		menu.setCompany("CIA");
		menu.setGroups(new LinkedHashSet<>());
		menu.getGroups().add(grp);
		menu.setServices(new LinkedHashSet<>());
		menu.getServices().add(srv);
		menu.setNames(new LinkedHashSet<>());
		menu.getNames().add(i18n);
		menu.setDescriptions(new LinkedHashSet<>());
		menu.getDescriptions().add(i18n);
		menu.setCodes(Set.of(pairService, pairGroup));

		return menu;
	}

	public static MasterItem master() {
		PairValue pair1 = new PairValue();
		pair1.setKey(MasterType.COMPANY.name());
		pair1.setValue(new LinkedHashSet<>(Set.of("1")));

		I18n i18n = new I18n();
		i18n.setKey("ES");
		i18n.setValue("test");

		MasterItem master = new MasterItem();
		master.setId("1");
		master.setNames(new LinkedHashSet<>());
		master.getNames().add(i18n);
		master.setEntity(MasterType.COMPANY.name());
		master.setCodes(Set.of(pair1));

		return master;
	}

	public static KafkaItem message() {
		KafkaItem kafka = new KafkaItem();
		kafka.setKey("key");
		kafka.setEntity("entity");
		kafka.setAction("test");
		kafka.setJson("json");

		return kafka;
	}

	public static void redisKey(final Class<?> classes, final Object obj, final String fieldName) {
		try {
			Field field = classes.getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(obj, "key");
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static void init(final Class<?> clazz, final Object obj, final String fieldName, final String fieldValue) {
		if (StringUtils.hasText(fieldName)) {
			try {
				Field field = clazz.getDeclaredField(fieldName);
				field.setAccessible(true);
				field.set(obj, fieldValue);
			} catch (NoSuchFieldException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}