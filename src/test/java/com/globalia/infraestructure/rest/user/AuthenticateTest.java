package com.globalia.infraestructure.rest.user;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.user.SaveUser;
import com.globalia.application.service.user.User;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.security.AESPassEncrypter;
import com.globalia.security.JwtTokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class AuthenticateTest extends HelperTest {

	@InjectMocks
	private Authenticate authenticate;
	@Mock
	private User user;
	@Mock
	private SaveUser save;
	@Mock
	private JwtTokenProvider jwtTokenProvider;
	@Mock
	private AESPassEncrypter encrypter;
	@Mock
	private final AuthenticationManager authenticationManager = authentication -> null;

	private Authentication authentication;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		jwtSecret(Authenticate.class, this.authenticate);
	}

	@Test
	public void testUserError() {
		ItemResponse user = new ItemResponse();
		user.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.user.getUser(anyString(), any())).thenReturn(user);
		ResponseEntity<ApiRS> response = this.authenticate.login(login());
		assertNotNull(response);
		assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
	}

	@Test
	public void testEncryptError() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		when(this.encrypter.encrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenThrow(NoSuchAlgorithmException.class);
		when(this.user.getUser(anyString(), any())).thenReturn(new ItemResponse());
		ResponseEntity<ApiRS> response = this.authenticate.login(login());
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testJWTError() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		ItemResponse item = new ItemResponse();
		item.setUser(user());

		when(this.jwtTokenProvider.createToken(any(), any(), any())).thenThrow(ValidateException.class);
		when(this.encrypter.encrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenReturn(item.getUser().getPass());
		when(this.user.getUser(anyString(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.authenticate.login(login());
		assertNotNull(response);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testChangePassError() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		ItemResponse itemNew = new ItemResponse();
		itemNew.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setPass(null);

		when(this.save.updateItem(any(), anyBoolean(), any())).thenReturn(itemNew);
		when(this.encrypter.encrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenReturn("changed");
		when(this.user.getUser(anyString(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.authenticate.login(login());
		assertNotNull(response);
		assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
	}

	@Test
	public void testChangeDiffPass() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		ItemResponse item = new ItemResponse();
		item.setUser(user());
		item.getUser().setPass("pass");

		when(this.save.updateItem(any(), anyBoolean(), any())).thenReturn(item);
		when(this.jwtTokenProvider.createToken(any(), any(), any())).thenReturn("token");
		when(this.encrypter.encrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenReturn("changed");
		when(this.user.getUser(anyString(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.authenticate.login(login());
		assertNotNull(response);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

	@Test
	public void testLogin() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		ItemResponse item = new ItemResponse();
		item.setUser(user());

		when(this.jwtTokenProvider.createToken(any(), any(), any())).thenReturn("token");
		when(this.encrypter.encrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenReturn(item.getUser().getPass());
		when(this.user.getUser(anyString(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.authenticate.login(login());
		assertNotNull(response);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}
}
