package com.globalia.infraestructure.rest.user;

import com.globalia.HelperTest;
import com.globalia.application.service.user.User;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.security.JwtTokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class RefreshTest extends HelperTest {

	@InjectMocks
	private Refresh refresh;
	@Mock
	private User user;
	@Mock
	private JwtTokenProvider jwtTokenProvider;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		jwtSecret(Refresh.class, this.refresh);
	}

	@Test
	public void testJWTUserError() {
		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenThrow(ValidateException.class);
		ResponseEntity<Void> response = this.refresh.refreshToken("token", login());
		assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
	}

	@Test
	public void testUserError() {
		ItemResponse user = new ItemResponse();
		user.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		ResponseEntity<Void> response = this.refresh.refreshToken("token", login());
		assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
	}

	@Test
	public void testRefresh() {
		ItemResponse user = new ItemResponse();
		user.setUser(user());

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		when(this.jwtTokenProvider.refreshToken(anyString(), any(), any(), any())).thenReturn("newToken");
		ResponseEntity<Void> response = this.refresh.refreshToken("token", login());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
}
