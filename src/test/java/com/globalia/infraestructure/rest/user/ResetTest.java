package com.globalia.infraestructure.rest.user;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.user.SaveUser;
import com.globalia.application.service.user.User;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.security.JwtTokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class ResetTest extends HelperTest {

	@InjectMocks
	private Reset reset;
	@Mock
	private User user;
	@Mock
	private SaveUser save;
	@Mock
	private JwtTokenProvider jwtTokenProvider;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		jwtSecret(Reset.class, this.reset);
	}

	@Test
	public void testJWTUserError() {
		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenThrow(ValidateException.class);
		ResponseEntity<ApiRS> response = this.reset.reset("token", login());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testUserError() {
		ItemResponse user = new ItemResponse();
		user.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		ResponseEntity<ApiRS> response = this.reset.reset("token", login());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void testSaveError() {
		ItemResponse user = new ItemResponse();
		user.setUser(user());

		ItemResponse item = new ItemResponse();
		item.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		when(this.save.updateItem(any(), anyBoolean(), any())).thenReturn(item);
		ResponseEntity<ApiRS> response = this.reset.reset("token", login());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void testReset() {
		ItemResponse user = new ItemResponse();
		user.setUser(user());

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		when(this.save.updateItem(any(), anyBoolean(), any())).thenReturn(user);
		ResponseEntity<ApiRS> response = this.reset.reset("token", login());
		assertEquals(HttpStatus.RESET_CONTENT, response.getStatusCode());
	}

}
