package com.globalia.infraestructure.rest.user;

import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.application.service.user.User;
import com.globalia.dto.login.ItemResponse;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.exception.ValidateException;
import com.globalia.security.AESPassEncrypter;
import com.globalia.security.JwtTokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class ValidateTest extends HelperTest {

	@InjectMocks
	private Validate validate;
	@Mock
	private User user;
	@Mock
	private JwtTokenProvider jwtTokenProvider;
	@Mock
	private AESPassEncrypter encrypter;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		jwtSecret(Validate.class, this.validate);
	}

	@Test
	public void testJWTUserError() {
		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenThrow(ValidateException.class);
		ResponseEntity<ApiRS> response = this.validate.validateToken("token", login());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testUserError() {
		ItemResponse user = new ItemResponse();
		user.setError(new Error("test", ErrorLayer.REPOSITORY_LAYER, HttpStatus.NO_CONTENT.value()));

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		ResponseEntity<ApiRS> response = this.validate.validateToken("token", login());
		assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
	}

	@Test
	public void testDecryptError() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		ItemResponse user = new ItemResponse();
		user.setUser(user());

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		when(this.encrypter.decrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenThrow(BadPaddingException.class);
		ResponseEntity<ApiRS> response = this.validate.validateToken("token", login());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
	}

	@Test
	public void testValidate() throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
		ItemResponse user = new ItemResponse();
		user.setUser(user());

		when(this.jwtTokenProvider.getUser(anyString(), any(), any())).thenReturn("token");
		when(this.user.getUser(anyString(), any())).thenReturn(user);
		when(this.encrypter.decrypt(anyString(), nullable(String.class), nullable(String.class), any(), nullable(LogType.class))).thenReturn(("pass"));
		ResponseEntity<ApiRS> response = this.validate.validateToken("token", login());
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
}
