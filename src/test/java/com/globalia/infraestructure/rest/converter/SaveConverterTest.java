package com.globalia.infraestructure.rest.converter;

import com.globalia.HelperTest;
import com.globalia.application.service.group.SaveGroup;
import com.globalia.application.service.master.SaveMaster;
import com.globalia.application.service.menu.SaveMenu;
import com.globalia.application.service.user.SaveUser;
import com.globalia.application.service.service.ServiceSave;
import com.globalia.dto.login.ItemResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SaveConverterTest extends HelperTest {

	@InjectMocks
	private SaveConverter result;
	@Mock
	private SaveUser saveUser;
	@Mock
	private SaveMenu saveMenu;
	@Mock
	private ServiceSave saveService;
	@Mock
	private SaveGroup saveGroup;
	@Mock
	private SaveMaster saveMaster;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testSaveError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error());
		when(this.saveUser.createItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.USER, SaveConverter.saveAction.CREATE, userRQ()));
	}

	@Test
	public void testSaveUser() {
		ItemResponse response = new ItemResponse();
		response.setUser(user());
		when(this.saveUser.createItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.USER, SaveConverter.saveAction.CREATE, userRQ()));
	}

	@Test
	public void testSaveMenu() {
		ItemResponse response = new ItemResponse();
		response.setMenu(menu());
		when(this.saveMenu.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.MENU, SaveConverter.saveAction.UPDATE, menuRQ()));
	}

	@Test
	public void testSaveGroup() {
		ItemResponse response = new ItemResponse();
		response.setGroup(group());
		when(this.saveGroup.deleteItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.GROUP, SaveConverter.saveAction.DELETE, groupRQ()));
	}

	@Test
	public void testSaveService() {
		ItemResponse response = new ItemResponse();
		response.setService(service());
		when(this.saveService.createItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.SERVICE, SaveConverter.saveAction.CREATE, serviceRQ()));
	}

	@Test
	public void testSaveMaster() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());
		when(this.saveMaster.updateItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.save(ItemConverter.apiRequest.MASTER, SaveConverter.saveAction.UPDATE, masterRQ()));
	}
}
