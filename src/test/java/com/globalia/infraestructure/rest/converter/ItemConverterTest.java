package com.globalia.infraestructure.rest.converter;

import com.globalia.HelperTest;
import com.globalia.application.service.group.Group;
import com.globalia.application.service.master.Master;
import com.globalia.application.service.menu.Menu;
import com.globalia.application.service.service.Service;
import com.globalia.application.service.user.User;
import com.globalia.dto.login.ItemResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ItemConverterTest extends HelperTest {

	@InjectMocks
	private ItemConverter result;
	@Mock
	private User user;
	@Mock
	private Menu menu;
	@Mock
	private Service service;
	@Mock
	private Group group;
	@Mock
	private Master master;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetItemError() {
		ItemResponse response = new ItemResponse();
		response.setError(new Error());
		when(this.user.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.USER, userRQ()));
	}

	@Test
	public void testGetItemUser() {
		ItemResponse response = new ItemResponse();
		response.setUser(user());
		when(this.user.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.USER, userRQ()));
	}

	@Test
	public void testGetItemMenu() {
		ItemResponse response = new ItemResponse();
		response.setMenu(menu());
		when(this.menu.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.MENU, menuRQ()));
	}

	@Test
	public void testGetItemGroup() {
		ItemResponse response = new ItemResponse();
		response.setGroup(group());
		when(this.group.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.GROUP, groupRQ()));
	}

	@Test
	public void testGetItemService() {
		ItemResponse response = new ItemResponse();
		response.setService(service());
		when(this.service.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.SERVICE, serviceRQ()));
	}

	@Test
	public void testGetItemMaster() {
		ItemResponse response = new ItemResponse();
		response.setMaster(master());
		when(this.master.getItem(any(), any())).thenReturn(response);
		assertNotNull(this.result.getItem(ItemConverter.apiRequest.MASTER, masterRQ()));
	}
}
