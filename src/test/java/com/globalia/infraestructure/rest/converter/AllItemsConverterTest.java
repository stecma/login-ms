package com.globalia.infraestructure.rest.converter;

import com.globalia.HelperTest;
import com.globalia.application.service.group.Groups;
import com.globalia.application.service.master.Masters;
import com.globalia.application.service.menu.Menus;
import com.globalia.application.service.user.Users;
import com.globalia.dto.login.AllItemsResponse;
import com.globalia.exception.Error;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AllItemsConverterTest extends HelperTest {

	@InjectMocks
	private AllItemsConverter result;
	@Mock
	private Users users;
	@Mock
	private Menus menus;
	@Mock
	private Groups groups;
	@Mock
	private Masters masters;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetAllItemsError() {
		AllItemsResponse response = new AllItemsResponse();
		response.setError(new Error());
		when(this.users.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.USER, userRQ()));
	}

	@Test
	public void testGetAllItemsUser() {
		AllItemsResponse response = new AllItemsResponse();
		response.setUsers(new LinkedHashSet<>(Set.of(user())));
		when(this.users.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.USER, userRQ()));
	}

	@Test
	public void testGetAllItemsMenu() {
		AllItemsResponse response = new AllItemsResponse();
		response.setMenus(new LinkedHashSet<>(Set.of(menu())));
		when(this.menus.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.MENU, menuRQ()));
	}

	@Test
	public void testGetAllItemsGroup() {
		AllItemsResponse response = new AllItemsResponse();
		response.setGroups(new LinkedHashSet<>(Set.of(group())));
		when(this.groups.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.GROUP, groupRQ()));
	}

	@Test
	public void testGetAllItemsService() {
		AllItemsResponse response = new AllItemsResponse();
		response.setServices(new LinkedHashSet<>(Set.of(service())));
		when(this.services.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.SERVICE, serviceRQ()));
	}

	@Test
	public void testGetAllItemsMaster() {
		AllItemsResponse response = new AllItemsResponse();
		response.setMasters(new LinkedHashSet<>(Set.of(master())));
		when(this.masters.getAllItems(any(), any())).thenReturn(response);
		assertNotNull(this.result.getAllItems(ItemConverter.apiRequest.MASTER, masterRQ()));
	}
}
