package com.globalia.infraestructure.rest.master;

import com.globalia.api.ApiRS;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class TypesTest {

	@InjectMocks
	private Types types;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetMasterType() {
		ResponseEntity<ApiRS> response = this.types.getMasterType();
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
}
