package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.master.Masters;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class ItemAspectTest extends HelperTest {

	@InjectMocks
	private ItemAspect interceptor;
	@Mock
	private Masters masters;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerUserNullRequest() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "user", null);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerNullUser() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "user", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerMenuNullRequest() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "menu", null);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerNullMenu() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "menu", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerGroupNullRequest() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "group", null);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerNullGroup() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "group", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerServiceNullRequest() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "service", null);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerNullService() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "service", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerMasterNullRequest() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "master", null);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerNullMaster() throws Throwable {
		this.interceptor.itemController(this.joinPoint, "master", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerUserNoId() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setId(null);

		this.interceptor.itemController(this.joinPoint, "user", item);
	}

	@Test
	public void testItemControllerUser() throws Throwable {
		ApiRQ item = userRQ();

		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.itemController(this.joinPoint, "user", item));
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerMenuNoID() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setId(null);

		this.interceptor.itemController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerMenuInvalidEnv() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(false);
		this.interceptor.itemController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerMenuInvalidCia() throws Throwable {
		ApiRQ item = menuRQ();
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(false);
		this.interceptor.itemController(this.joinPoint, "menu", item);
	}

	@Test
	public void testItemControllerMenu() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.itemController(this.joinPoint, "menu", item));
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerGroupNoId() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setId(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(false);
		this.interceptor.itemController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerGroupInvalidEnv() throws Throwable {
		ApiRQ item = groupRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(false);
		this.interceptor.itemController(this.joinPoint, "group", item);
	}

	@Test
	public void testItemControllerGroup() throws Throwable {
		ApiRQ item = groupRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.itemController(this.joinPoint, "group", item));
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerServiceNoId() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setId(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(false);
		this.interceptor.itemController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerServiceInvalidEnv() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(false);
		this.interceptor.itemController(this.joinPoint, "service", item);
	}

	@Test
	public void testItemControllerService() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.itemController(this.joinPoint, "service", item));
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerMasterNoID() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setId(null);
		this.interceptor.itemController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testItemControllerMasterNoEntity() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setEntity(null);
		this.interceptor.itemController(this.joinPoint, "master", item);
	}

	@Test
	public void testItemControllerMaster() throws Throwable {
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.itemController(this.joinPoint, "master", masterRQ()));
	}
}