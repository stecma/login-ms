package com.globalia.infraestructure.aspect;

import com.globalia.api.login.ApiRQ;
import com.globalia.dto.login.LoginItem;
import com.globalia.exception.ValidateException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class LoginAspectTest {

	@InjectMocks
	private LoginAspect login;
	@Mock
	private ProceedingJoinPoint joinPoint;
	@Mock
	private MethodSignature signature;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test(expected = ValidateException.class)
	public void testLoginControllerTokenNullRequest() throws Throwable {
		this.login.loginControllerToken(this.joinPoint, "asdas", null);
	}

	@Test(expected = ValidateException.class)
	public void testLoginControllerTokenNullToken() throws Throwable {
		this.login.loginControllerToken(this.joinPoint, null, new ApiRQ());
	}

	@Test
	public void testLoginControllerToken() throws Throwable {
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.login.loginControllerToken(this.joinPoint, "asdas", new ApiRQ()));
	}

	@Test(expected = ValidateException.class)
	public void testLoginControllerNullRequest() throws Throwable {
		this.login.loginController(this.joinPoint, null);
	}

	@Test(expected = ValidateException.class)
	public void testLoginControllerNullLoginRequest() throws Throwable {
		this.login.loginController(this.joinPoint, new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testLoginControllerEmptyUser() throws Throwable {
		ApiRQ request = new ApiRQ();
		request.setLogin(new LoginItem());

		this.login.loginController(this.joinPoint, request);
	}

	@Test(expected = ValidateException.class)
	public void testLoginControllerEmptyPass() throws Throwable {
		ApiRQ request = new ApiRQ();
		request.setLogin(new LoginItem());
		request.getLogin().setUser("test");

		this.login.loginController(this.joinPoint, request);
	}

	@Test
	public void testLoginController() throws Throwable {
		ApiRQ request = new ApiRQ();
		request.setLogin(new LoginItem());
		request.getLogin().setUser("test");
		request.getLogin().setPass("test");

		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.login.loginController(this.joinPoint, request));
	}
}