package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.master.Masters;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class AllItemsAspectTest extends HelperTest {

	@InjectMocks
	private AllItemsAspect interceptor;
	@Mock
	private Masters masters;


	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		Mockito.when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerUserNullRequest() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "user", null);
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerNullUser() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "user", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerMenuNullRequest() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "menu", null);
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerNullMenu() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "menu", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerGroupNullRequest() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "group", null);
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerNullGroup() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "group", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerServiceNullRequest() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "service", null);
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerNullService() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "service", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerMasterNullRequest() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "master", null);
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerNullMaster() throws Throwable {
		this.interceptor.allItemsController(this.joinPoint, "master", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerMenuInvalidEnvironment() throws Throwable {
		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(false);
		this.interceptor.allItemsController(this.joinPoint, "menu", menuRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerMenuInvalidCia() throws Throwable {
		ApiRQ request = menuRQ();

		Mockito.when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), request.getMenu().getEnvironment())).thenReturn(true);
		Mockito.when(this.masters.validateKey(MasterType.COMPANY.name(), request.getMenu().getCompany())).thenReturn(false);
		this.interceptor.allItemsController(this.joinPoint, "menu", menuRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerGroupInvalidEnvironment() throws Throwable {
		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(false);
		this.interceptor.allItemsController(this.joinPoint, "group", groupRQ());
	}

	@Test(expected = ValidateException.class)
	public void testAllItemsControllerServiceInvalidEnvironment() throws Throwable {
		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(false);
		this.interceptor.allItemsController(this.joinPoint, "service", serviceRQ());
	}

	@Test
	public void testAllItemsControllerMenu() throws Throwable {
		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(true);
		Mockito.when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.allItemsController(this.joinPoint, "menu", menuRQ()));
	}

	@Test
	public void testAllItemsControllerEmptyEnvGroup() throws Throwable {
		ApiRQ request = groupRQ();
		request.getGroup().setEnvironment(null);
		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(true);
		Mockito.when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.allItemsController(this.joinPoint, "group", request));
	}

	@Test
	public void testAllItemsControllerGroup() throws Throwable {
		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(true);
		Mockito.when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.allItemsController(this.joinPoint, "group", groupRQ()));
	}

	@Test
	public void testAllItemsControllerEmptyEnvService() throws Throwable {
		ApiRQ request = serviceRQ();
		request.getService().setEnvironment(null);

		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(true);
		Mockito.when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.allItemsController(this.joinPoint, "service", request));
	}

	@Test
	public void testAllItemsControllerService() throws Throwable {
		ApiRQ request = serviceRQ();
		request.getService().setDescriptions(null);
		Mockito.when(this.masters.validateKey(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(true);
		Mockito.when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.allItemsController(this.joinPoint, "service", serviceRQ()));
	}

	@Test
	public void testAllItemsControllerMaster() throws Throwable {
		Mockito.when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.allItemsController(this.joinPoint, "master", masterRQ()));
	}
}