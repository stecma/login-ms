package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.login.MasterItem;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CreateAspectTest extends HelperTest {

	@InjectMocks
	private CreateAspect interceptor;
	@Mock
	private Masters masters;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNullRequest() throws Throwable {
		this.interceptor.createController(this.joinPoint, "user", null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerNullUser() throws Throwable {
		this.interceptor.createController(this.joinPoint, "user", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuNullRequest() throws Throwable {
		this.interceptor.createController(this.joinPoint, "menu", null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerNullMenu() throws Throwable {
		this.interceptor.createController(this.joinPoint, "menu", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerGroupNullRequest() throws Throwable {
		this.interceptor.createController(this.joinPoint, "group", null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerNullGroup() throws Throwable {
		this.interceptor.createController(this.joinPoint, "group", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerServiceNullRequest() throws Throwable {
		this.interceptor.createController(this.joinPoint, "service", null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerNullService() throws Throwable {
		this.interceptor.createController(this.joinPoint, "service", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMasterNullRequest() throws Throwable {
		this.interceptor.createController(this.joinPoint, "master", null);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerNullMaster() throws Throwable {
		this.interceptor.createController(this.joinPoint, "master", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNoId() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setId(null);

		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNoName() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setName(null);

		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNoSurName() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setSurname(null);

		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNoLanguage() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setLanguage(null);

		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNoMail() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setMail(null);

		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserInvaligLang() throws Throwable {
		ApiRQ item = userRQ();
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNoCias() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setCompanies(null);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserEmptyNoCias() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserInvalidCias() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserNoEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(null);

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserEmptyEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerUserInvalidEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), env.getId())).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "user", item);
	}

	@Test
	public void testCreateControllerUser() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.createController(this.joinPoint, "user", item));
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuNoEnv() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setEnvironment(null);

		this.interceptor.createController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuInvalidEnv() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuNoCia() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setCompany(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuInvalidCia() throws Throwable {
		ApiRQ item = menuRQ();
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuNoName() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuEmptyName() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMenuInvalidName() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "menu", item);
	}

	@Test
	public void testCreateControllerMenu() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.createController(this.joinPoint, "menu", item));
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerGroupInvalidEnv() throws Throwable {
		ApiRQ item = groupRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerGroupNoName() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerGroupEmptyName() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "group", item);
	}

	@Test
	public void testCreateControllerGroup() throws Throwable {
		ApiRQ item = groupRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.createController(this.joinPoint, "group", item));
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerServiceInvalidEnv() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(false);
		this.interceptor.createController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerServiceInvalidURL() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setUrl(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerServiceInvalidRoute() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().getUrl().setRoute(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerServiceNoName() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerServiceEmptyName() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.createController(this.joinPoint, "service", item);
	}

	@Test
	public void testCreateControllerService() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.createController(this.joinPoint, "service", item));
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMasterNoEntity() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setEntity(null);
		this.interceptor.createController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMasterNoName() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setNames(null);

		this.interceptor.createController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testCreateControllerMasterEmptyName() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setNames(new LinkedHashSet<>());

		this.interceptor.createController(this.joinPoint, "master", item);
	}

	@Test
	public void testCreateControllerMaster() throws Throwable {
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.createController(this.joinPoint, "master", masterRQ()));
	}
}