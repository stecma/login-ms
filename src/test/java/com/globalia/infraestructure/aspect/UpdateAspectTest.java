package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class UpdateAspectTest extends HelperTest {

	@InjectMocks
	private UpdateAspect interceptor;
	@Mock
	private Masters masters;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNullRequest() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "user", null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerNullUser() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "user", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuNullRequest() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "menu", null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerNullMenu() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "menu", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerGroupNullRequest() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "group", null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerNullGroup() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "group", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerServiceNullRequest() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "service", null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerNullService() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "service", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMasterNullRequest() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "master", null);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerNullMaster() throws Throwable {
		this.interceptor.updateController(this.joinPoint, "master", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoId() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setId(null);

		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoName() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setName(null);

		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoSurName() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setSurname(null);

		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoLanguage() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setLanguage(null);

		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoMail() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setMail(null);

		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserInvaligLang() throws Throwable {
		ApiRQ item = userRQ();
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoCias() throws Throwable {
		ApiRQ item = userRQ();
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserEmptyNoCias() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserInvalidCias() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserEmptyEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserInvalidEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), env.getId())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserNoConfig() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserEmptyConfig() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>());

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserInvalidMenuEnv() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		UserMenu menu = new UserMenu();
		menu.setEnvironment("ENV");
		menu.setCompany("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>(Set.of(menu)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerUserInvalidMenuCia() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("ENV");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("ENV");

		UserMenu menu = new UserMenu();
		menu.setEnvironment("ENV");
		menu.setCompany("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>(Set.of(menu)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "user", item);
	}

	@Test
	public void testUpdateControllerUser() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("ENV");

		UserMenu menu = new UserMenu();
		menu.setEnvironment("ENV");
		menu.setCompany("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>(Set.of(menu)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.updateController(this.joinPoint, "user", item));
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuNoID() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setId(null);

		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuNoEnv() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setEnvironment(null);

		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuInvalidEnv() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuNoCia() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setCompany(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuInvalidCia() throws Throwable {
		ApiRQ item = menuRQ();
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuNoName() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuEmptyName() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMenuInvalidName() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "menu", item);
	}

	@Test
	public void testUpdateControllerMenu() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.updateController(this.joinPoint, "menu", item));
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerGroupNoId() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setId(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerGroupInvalidEnv() throws Throwable {
		ApiRQ item = groupRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerGroupNoName() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerGroupEmptyName() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "group", item);
	}

	@Test
	public void testUpdateControllerGroup() throws Throwable {
		ApiRQ item = groupRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.updateController(this.joinPoint, "group", item));
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerServiceNoId() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setId(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerServiceInvalidEnv() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(false);
		this.interceptor.updateController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerServiceInvalidURL() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setUrl(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerServiceInvalidRoute() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().getUrl().setRoute(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerServiceNoName() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerServiceEmptyName() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.updateController(this.joinPoint, "service", item);
	}

	@Test
	public void testUpdateControllerService() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.updateController(this.joinPoint, "service", item));
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMasterNoID() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setId(null);
		this.interceptor.updateController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMasterNoEntity() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setEntity(null);
		this.interceptor.updateController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMasterNoName() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setNames(null);

		this.interceptor.updateController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testUpdateControllerMasterEmptyName() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setNames(new LinkedHashSet<>());

		this.interceptor.updateController(this.joinPoint, "master", item);
	}

	@Test
	public void testUpdateControllerMaster() throws Throwable {
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.updateController(this.joinPoint, "master", masterRQ()));
	}
}