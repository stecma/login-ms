package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import com.globalia.api.login.ApiRQ;
import com.globalia.application.service.master.Masters;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.UserMenu;
import com.globalia.enumeration.login.MasterType;
import com.globalia.exception.ValidateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.LinkedHashSet;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SuppressWarnings("unused")
@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class DeleteAspectTest extends HelperTest {

	@InjectMocks
	private DeleteAspect interceptor;
	@Mock
	private Masters masters;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNullRequest() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "user", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullUser() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "user", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuNullRequest() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "menu", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullMenu() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "menu", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerGroupNullRequest() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "group", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullGroup() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "group", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerServiceNullRequest() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "service", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullService() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "service", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMasterNullRequest() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "master", null);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerNullMaster() throws Throwable {
		this.interceptor.deleteController(this.joinPoint, "master", new ApiRQ());
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoId() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setId(null);

		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoName() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setName(null);

		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoSurName() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setSurname(null);

		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoLanguage() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setLanguage(null);

		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoMail() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setMail(null);

		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserInvaligLang() throws Throwable {
		ApiRQ item = userRQ();
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoCias() throws Throwable {
		ApiRQ item = userRQ();
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserEmptyNoCias() throws Throwable {
		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserInvalidCias() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserEmptyEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserInvalidEnvs() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));

		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), item.getUser().getLanguage())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), cia.getId())).thenReturn(true);
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), env.getId())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserNoConfig() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(null);

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserEmptyConfig() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>());

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserInvalidMenuEnv() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("CIA");

		UserMenu menu = new UserMenu();
		menu.setEnvironment("ENV");
		menu.setCompany("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>(Set.of(menu)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerUserInvalidMenuCia() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("ENV");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("ENV");

		UserMenu menu = new UserMenu();
		menu.setEnvironment("ENV");
		menu.setCompany("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>(Set.of(menu)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "user", item);
	}

	@Test
	public void testDeleteControllerUser() throws Throwable {
		MasterItem cia = new MasterItem();
		cia.setEntity(MasterType.COMPANY.name());
		cia.setId("CIA");

		MasterItem env = new MasterItem();
		env.setEntity(MasterType.ENVIRONMENT.name());
		env.setId("ENV");

		UserMenu menu = new UserMenu();
		menu.setEnvironment("ENV");
		menu.setCompany("CIA");

		ApiRQ item = userRQ();
		item.getUser().setCompanies(new LinkedHashSet<>(Set.of(cia)));
		item.getUser().setEnvironments(new LinkedHashSet<>(Set.of(env)));
		item.getUser().setConfiguration(new LinkedHashSet<>(Set.of(menu)));

		when(this.masters.validateKey(anyString(), anyString())).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.deleteController(this.joinPoint, "user", item));
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuNoID() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setId(null);

		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuNoEnv() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setEnvironment(null);

		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuInvalidEnv() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuNoCia() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setCompany(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuInvalidCia() throws Throwable {
		ApiRQ item = menuRQ();
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuNoName() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuEmptyName() throws Throwable {
		ApiRQ item = menuRQ();
		item.getMenu().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMenuInvalidName() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "menu", item);
	}

	@Test
	public void testDeleteControllerMenu() throws Throwable {
		ApiRQ item = menuRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getMenu().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.COMPANY.name(), item.getMenu().getCompany())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.deleteController(this.joinPoint, "menu", item));
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerGroupNoId() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setId(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerGroupInvalidEnv() throws Throwable {
		ApiRQ item = groupRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerGroupNoName() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "group", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerGroupEmptyName() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "group", item);
	}

	@Test
	public void testDeleteControllerGroup() throws Throwable {
		ApiRQ item = groupRQ();
		item.getGroup().setDescriptions(new LinkedHashSet<>());
		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getGroup().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.deleteController(this.joinPoint, "group", item));
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerServiceNoId() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setId(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerServiceInvalidEnv() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(false);
		this.interceptor.deleteController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerServiceInvalidURL() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setUrl(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerServiceInvalidRoute() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().getUrl().setRoute(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerServiceNoName() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setNames(null);

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "service", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerServiceEmptyName() throws Throwable {
		ApiRQ item = serviceRQ();
		item.getService().setNames(new LinkedHashSet<>());

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		this.interceptor.deleteController(this.joinPoint, "service", item);
	}

	@Test
	public void testDeleteControllerService() throws Throwable {
		ApiRQ item = serviceRQ();

		when(this.masters.validateKey(MasterType.ENVIRONMENT.name(), item.getService().getEnvironment())).thenReturn(true);
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.deleteController(this.joinPoint, "service", item));
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMasterNoID() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setId(null);
		this.interceptor.deleteController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMasterNoEntity() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setEntity(null);
		this.interceptor.deleteController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMasterNoName() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setNames(null);

		this.interceptor.deleteController(this.joinPoint, "master", item);
	}

	@Test(expected = ValidateException.class)
	public void testDeleteControllerMasterEmptyName() throws Throwable {
		ApiRQ item = masterRQ();
		item.getMaster().setNames(new LinkedHashSet<>());

		this.interceptor.deleteController(this.joinPoint, "master", item);
	}

	@Test
	public void testDeleteControllerMaster() throws Throwable {
		when(this.masters.validateKey(MasterType.LANGUAGES_APP.name(), "ES")).thenReturn(true);
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.interceptor.deleteController(this.joinPoint, "master", masterRQ()));
	}
}