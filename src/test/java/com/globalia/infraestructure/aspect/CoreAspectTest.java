package com.globalia.infraestructure.aspect;

import com.globalia.HelperTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class CoreAspectTest extends HelperTest {

	@InjectMocks
	private CoreAspect coreAspect;

	@Before
	public void setUp() {
		MockitoAnnotations.openMocks(this);
		when(this.joinPoint.getSignature()).thenReturn(this.signature);
	}

	@Test
	public void testLoggerServiceSelect() throws Throwable {
		assertNotNull(this.getLoggerService("getItem"));
	}

	@Test
	public void testLoggerServiceCreate() throws Throwable {
		assertNotNull(this.getLoggerService("create"));
	}

	@Test
	public void testLoggerServiceUpdate() throws Throwable {
		assertNotNull(this.getLoggerService("update"));
	}

	@Test
	public void testLoggerServiceDelete() throws Throwable {
		assertNotNull(this.getLoggerService("delete"));
	}

	@Test
	public void testLoggerRedisSelect() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("getItem");
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.coreAspect.loggerRedis(this.joinPoint, monitor()));
	}

	@Test
	public void testLoggerRedisAddItem() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("addItem");
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.coreAspect.loggerRedis(this.joinPoint, monitor()));
	}

	@Test
	public void testLoggerRedisAddItemNoMonitor() throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn("addItem");
		when(this.joinPoint.proceed()).thenReturn("OK");
		assertNotNull(this.coreAspect.loggerRedis(this.joinPoint, null));
	}

	private Object getLoggerService(final String method) throws Throwable {
		when(this.joinPoint.getSignature().getName()).thenReturn(method);
		when(this.joinPoint.proceed()).thenReturn("OK");
		return this.coreAspect.loggerService(this.joinPoint, monitor());
	}
}